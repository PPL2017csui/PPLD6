// ===================================
// Core Dependencies
// ===================================
import Vue from 'vue'
import Router from 'vue-router'
import { store } from '../vuex'

// ===================================
// Components
// ===================================
import Landing from '../components/Landing.vue'
import Login from '../components/Login.vue'
import Dashboard from '../components/Dashboard.vue'
import Error404 from '../components/Error404.vue'
import Inactive from '../components/Inactive.vue'
// Forms
import FormsTemplate from '../components/form/Template.vue'
import FormsCreate from '../components/form/Create.vue'
import FormsList from '../components/form/List.vue'
import FormsEdit from '../components/form/Edit.vue'
import FormsShow from '../components/form/Show.vue'
import FormsShowVersion from '../components/form/ShowVersion.vue'
// Users
import UsersTemplate from '../components/user/Template.vue'
import UsersCreate from '../components/user/Create.vue'
import UsersList from '../components/user/List.vue'
import UsersEdit from '../components/user/Edit.vue'
import UsersShow from '../components/user/Show.vue'
// Reports
import ReportsTemplate from '../components/report/Template.vue'
import ReportsList from '../components/report/List.vue'
import ReportsByUser from '../components/report/ByUser.vue'

// ===================================
// Register VueRouter to Vue
// ===================================
Vue.use(Router)

export const router = new Router({
  routes: [
    { path: '*', name: 'not-found', component: Error404 },
    { path: '/', name: '/landing', component: Landing },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        redirectIfLoggedIn: {
          name: 'dashboard'
        }
      }
    },
    {
      path: '/inactive',
      name: 'inactive',
      component: Inactive
    },
    {
      path: '/register',
      name: 'users.create',
      component: UsersCreate
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        adminAuth: true
      }
    },
    {
      path: '/users',
      component: UsersTemplate,
      meta: {
        adminAuth: true
      },
      children: [
        {
          path: '',
          name: 'users.list',
          component: UsersList
        },
        {
          path: ':id/edit',
          name: 'users.edit',
          component: UsersEdit
        },
        {
          path: ':id',
          name: 'users.show',
          component: UsersShow
        }
      ]
    },
    {
      path: '/forms',
      component: FormsTemplate,
      meta: {
        adminAuth: true
      },
      children: [
        {
          path: '',
          name: 'forms.list',
          component: FormsList
        },
        {
          path: 'create',
          name: 'forms.create',
          component: FormsCreate
        },
        {
          path: ':id',
          component: FormsShow,
          children: [
            {
              path: '',
              name: 'forms.show',
              component: FormsShowVersion
            },
            {
              path: ':versionId',
              name: 'forms.version',
              component: FormsShowVersion
            }
          ]
        },
        {
          path: ':id/:versionId/edit',
          name: 'forms.edit',
          component: FormsEdit
        }
      ]
    },
    {
      path: '/reports',
      component: ReportsTemplate,
      meta: {
        adminAuth: true
      },
      children: [
        {
          path: '',
          name: 'reports.list',
          component: ReportsList
        },
        {
          path: 'user',
          name: 'reports.byUser',
          component: ReportsByUser
        }
      ]
    }
  ]
})

router.beforeEach((to, from, next) => {
  let user = store.getters.getUser
  let loginPath = {
    path: '/login',
    query: {
      redirect: to.fullPath
    }
  }
  let userPath = {
    path: '/inactive',
    query: {
      redirect: to.fullPath
    }
  }

  // If null then next()
  let redirectPath = null
  if (user && user.role === user && user.active === false) {
    redirectPath = userPath // implement notif
  } else if (to.matched.some(record => record.meta.adminAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!(user && user.role && user.role === 'admin')) {
      redirectPath = loginPath
    }
  } else if (to.matched.some(record => record.meta.userAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    if (!(user && user.role && (user.role === 'admin' || user.role === 'user'))) {
      redirectPath = loginPath
    }
  } else if (to.matched.some(record => record.meta.redirectIfLoggedIn)) {
    if (user && user.role) {
      redirectPath = to.meta.redirectIfLoggedIn
    }
  }

  if (redirectPath) {
    next(redirectPath)
  } else {
    next()
  }
})
