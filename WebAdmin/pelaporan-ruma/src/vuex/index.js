import Vue from 'vue'
import Vuex from 'vuex'
import firebase from 'firebase'
import { router } from '../router'

// Initialize Firebase
var config = {
  apiKey: 'AIzaSyDt51PvfVYMzcp4hXepPyv0Bv49qbhWyPg',
  authDomain: 'marumaru-b2f9a.firebaseapp.com',
  databaseURL: 'https://marumaru-b2f9a.firebaseio.com',
  projectId: 'marumaru-b2f9a',
  storageBucket: 'marumaru-b2f9a.appspot.com',
  messagingSenderId: '57184390212'
}
firebase.initializeApp(config)

const auth = firebase.auth()

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    auth: auth,
    database: firebase.database(),
    storage: firebase.storage(),
    user: null,
    users: [],
    formTemplates: [],
    loading: true,
    loadingText: 'Loading your apps'
  },
  getters: {
    // ========= GLOBALLY USABLE =========
    isLoading: state => {
      return state.loading
    },
    getLoadingText: state => {
      return state.loadingText
    },
    // ========= FIREBASE =========
    getDatabaseInstance: state => {
      return state.database
    },
    // =========== USER DOMAIN ===========
    getUser: state => {
      return state.user
    },
    getUsers: state => {
      return state.users
    },
    getUserTemplateById: (state, getters) => (id) => {
      return state.users[id]
    },
    // =========== FORM DOMAIN ===========
    getFormTemplates: state => {
      return state.formTemplates
    },
    getFormTemplateById: (state, getters) => (id) => {
      return state.formTemplates[id]
    },
    getActiveVersion: (state, getters) => (id) => {
      var form = getters.getFormTemplateById(id)
      if (form.active === 0) {
        // Get the last version
        let numberOfVersion = form.versions.length
        console.log(form.versions[numberOfVersion - 1])
        return form.versions[numberOfVersion - 1]
      }
      return form.versions[form.active]
    },
    getFormTemplateByIdAndVersion: (state, getters) => (id, versionId) => {
      var form = getters.getFormTemplateById(id)
      return form.versions[versionId]
    },
    getLatestVersionOfFormTemplate: (state, getters) => (id) => {
      var formTemplate = getters.getFormTemplateById(id)
      return formTemplate.versions.length - 1
    }
  },
  mutations: {
    // ========= GLOBALLY USABLE =========
    stopLoading: (state) => {
      state.loading = false
    },
    startLoading: (state) => {
      state.loading = true
      state.loadingText = ''
    },
    startLoadingWithText: (state, text) => {
      state.loading = true
      state.loadingText = text
    },
    // =========== USER DOMAIN ===========
    setUser: (state, user) => {
      state.user = user
    },
    addUserTemplates: (state, user) => {
      Vue.set(state.users, user.id, user)
    },
    // =========== FORM DOMAIN ===========
    setFormTemplates: (state, formTemplates) => {
      state.formTemplates = formTemplates
    },
    addFormTemplates: (state, formTemplate) => {
      Vue.set(state.formTemplates, formTemplate.id.toString(), formTemplate)
    },
    removeFormTemplates: (state, formTemplate) => {
      Vue.delete(state.formTemplates, formTemplate[1].id)
    },
    activateVersionAndDeactivateOther: (state, params) => {
      console.log(params)
      console.log(state.formTemplates)
      var db = state.database
      Object.keys(state.formTemplates).forEach(function (key) {
        var active = 0
        if (key === params.formId) {
          active = parseInt(params.formVersion)
        }
        db.ref('form_templates/' + key + '/active').set(active).then(function (snapshot) {
          console.log(snapshot)
        })
      })
    }

  },
  actions: {
    // ====== AUTHENTICATION DOMAIN =======
    authStateChanged (context, user) {
      console.log('vuex:authStateChanged::start')

      if (user) { // User is signed in!
        console.log('vuex:authStateChanged::user_signed_in')
        let ctx = context

        context.commit('startLoadingWithText', 'Authenticated, getting user\'s data')
        context.state.database.ref('users/' + user.uid)
          .once('value').then(function (snapshot) {
            if (!snapshot.val() || snapshot.val().role === 'user') {
              ctx.dispatch('logout', 'inactive')
              router.push('inactive')
              console.log('inactive')
            } else {
              ctx.commit('setUser', snapshot.val())
              router.push('dashboard')
              console.log('dashboard')
              console.log(snapshot.val().role)
            }
            ctx.commit('stopLoading')
          })
      } else { // User is signed out!
        console.log('vuex:authStateChanged::user_signed_out')
        context.commit('setUser', null)
        context.commit('stopLoading')
      }

      console.log('vuex:authStateChanged::finish')
    },
    login (context, user) {
      console.log('vuex:login::start')
      context.commit('startLoadingWithText', 'Logging you in')

      user.email = user.email + '@marumaru.com'
      context.state.auth.signInWithEmailAndPassword(user.email, user.password).catch(function (error) {
        // Handle Errors here.
        console.log(error)
        var errorCode = error.code
        var errorMessage = error.message
        alert(errorCode)
        alert(errorMessage)
        context.commit('stopLoading')
      }).then(function () {
        console.log('vuex:login::finish')
      })
    },
    logout (context, status) {
      console.log('vuex:logout:start')
      context.commit('startLoadingWithText', 'Logging you out')

      context.state.auth.signOut().then(function () {
        context.state.user = null
        if (status) {
          router.push('inactive')
        } else {
          router.push('/')
        }
      }).catch(function (error) {
        alert(error.message)
      })

      console.log('vuex:logout:finish')
    }
  }
})

auth.onAuthStateChanged(function (user) {
  store.dispatch('authStateChanged', user)
})
