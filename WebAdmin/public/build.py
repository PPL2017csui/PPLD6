from sys import platform
from subprocess import call

if platform == "linux" or platform == "linux2":
    call(["xdg-open", "index.html"])
elif platform == "darwin":
    call(["open", "index.html"])
elif platform == "win32":
    call(["start", "index.html"])
