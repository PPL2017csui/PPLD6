'use strict';

// Initializes Pelaporan.
function Pelaporan() {
  this.checkSetup();
  // NAVIGATIONS =========================================
  this.loginNav      = document.getElementById('login_nav');

  // PAGES ===============================================
  this.loginView     = document.getElementById('login_view');
  this.dashboardView     = document.getElementById('dashboard_view');

  // LOGIN - LOGOUT ======================================
  // Shortcuts to DOM Elements.
  this.loginEmail    = document.getElementById('login_email');
  this.loginPassword = document.getElementById('login_password');
  this.loginButton   = document.getElementById('login_button');
  this.logoutButton  = document.getElementById('logout_button');
  // Saves message on form submit.
  this.loginButton.addEventListener('click', this.login.bind(this));
  this.logoutButton.addEventListener('click', this.logout.bind(this));

  // USER ======================================
  // Shortcuts to DOM Elements.
  this.userDropdown  = document.getElementById('user_dropdown');
  this.userName      = document.getElementById('user_name');
  // Saves message on form submit.

  // Toggle for the button.
  // var buttonTogglingHandler = this.toggleButton.bind(this);
  // this.messageInput.addEventListener('keyup', buttonTogglingHandler);
  // this.messageInput.addEventListener('change', buttonTogglingHandler);

  this.initFirebase();
}

// Sets up shortcuts to Firebase features and initiate firebase auth.
Pelaporan.prototype.initFirebase = function() {
  // Shortcuts to Firebase SDK features.
  this.auth = firebase.auth();
  this.database = firebase.database();
  this.storage = firebase.storage();
  // Initiates Firebase auth and listen to auth state changes.
  this.auth.onAuthStateChanged(this.onAuthStateChanged.bind(this));
};


// LOGIN - LOGOUT =========================================
Pelaporan.prototype.login = function () {
  var email = this.loginEmail.value + '@marumaru.com';
  var password = this.loginPassword.value;
  firebase.auth().signInWithEmailAndPassword(email, password).catch(function(error) {
    // Handle Errors here.
    console.log(error);
    var errorCode = error.code;
    var errorMessage = error.message;
    alert(errorCode);
    alert(errorMessage);
    // ...
  });
}

// action for logout activity
Pelaporan.prototype.logout = function () {
  firebase.auth().signOut().then(function() {
    // Sign-out successful.
  }).catch(function(error) {
    alert(error.message);
  });
}

// Triggers when the auth state change for instance when the user signs-in or signs-out.
Pelaporan.prototype.onAuthStateChanged = function(user) {
  if (user) { // User is signed in!
    // Get profile pic and user's name from the Firebase user object.
    console.log(user);
    var profilePicUrl = user.photoURL;
    var userName = user.email;

    // Set the user's profile pic and name.
    this.userName.text = userName;

    // Hide sign-in button.
    this.loginButton.setAttribute('hidden', 'true');
    this.loginView.setAttribute('hidden', 'true');
    this.loginNav.setAttribute('hidden', 'true');

    // Show things
    this.userName.removeAttribute('hidden');
    this.logoutButton.removeAttribute('hidden');
    this.dashboardView.removeAttribute('hidden');
  } else { // User is signed out!
    this.userName.setAttribute('hidden', 'true');
    this.logoutButton.setAttribute('hidden', 'true');
    this.dashboardView.setAttribute('hidden', 'true');

    // Show things
    this.loginButton.removeAttribute('hidden');
    this.loginView.removeAttribute('hidden');
    this.loginNav.removeAttribute('hidden');
  }
};



// Checks that the Firebase SDK has been correctly setup and configured.
Pelaporan.prototype.checkSetup = function() {
  if (!window.firebase || !(firebase.app instanceof Function) || !window.config) {
    window.alert('You have not configured and imported the Firebase SDK.');
  } else if (config.storageBucket === '') {
    window.alert('Your Cloud Storage bucket has not been enabled. Sorry about that. This is ' +
        'actually a Firebase bug that occurs rarely. ' +
        'Please go and re-generate the Firebase initialisation snippet (step 4 of the codelab) ' +
        'and make sure the storageBucket attribute is not empty. ' +
        'You may also need to visit the Storage tab and paste the name of your bucket which is ' +
        'displayed there.');
  }
};

window.onload = function() {
  window.Pelaporan = new Pelaporan();
};
