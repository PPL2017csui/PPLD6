# Pelaporan Ruma
Pelaporan Ruma merupakan aplikasi berbasis Android untuk membantu tim lapangan dari PT Ruma melakukan pelaporan yang cepat dan nyaman. Terdapat juga website administrasi untuk membantu penggunaan aplikasi ini dari tim pusat. Aplikasi ini merupakan proyek mata kuliah PPL dari Fakultas Ilmu Komputer Universitas Indonesia. 


## Client
- RUMA

## Scrum Master
- Prakash Divy

## Anggota Kelompok

- Edison Tantra
- Jefly
- Kelvin
- Maruli James
- Ricky Kurniawan

## Release Information 
Repo ini berisi kode untuk pengembangan aplikasi Pelaporan Ruma.

### Version 0.0 
- Fill Form (Android)
- Submit Form (Android)
- Dynamic Form Fields
- Add User (Web)

### Version 0.1 
- Save Draft (Android)
- Offline Support (Android)
- CRUD Form (Web)
- CRUD User (Web)

## Feature
- Fill Form (Android)
- Submit Form (Android)
- Dynamic Form Fields
- Add User (Web)
- Save Draft (Android)
- Offline Support (Android)
- CRUD Form (Web)
- CRUD User (Web)

P.S OnProcess(op)   