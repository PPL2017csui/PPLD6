package com.ppld6.marumaru.pelaporanruma;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.Intent;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;

import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.listForm.ListFormActivity;
import com.ppld6.marumaru.pelaporanruma.listForm.model.ListFormCardModel;
import com.ppld6.marumaru.pelaporanruma.login.LoginActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowToast;

import static junit.framework.Assert.assertEquals;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by jefly on 06/04/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class ListFormActivityTest {

    @Test
    public void checkButton(){
        ListFormActivity activity = Robolectric.setupActivity(ListFormActivity.class);
        activity.findViewById(R.id.btn_isi).performClick();
        Intent  intent = shadowOf(activity).getNextStartedActivity();
        ShadowIntent sIntent = shadowOf(intent);
        assertThat(sIntent.getIntentClass()).isEqualTo(FormActivity.class);
    }

    @Test
    public void checkLogout(){
        ListFormActivity activity = Robolectric.setupActivity(ListFormActivity.class);
        activity.logout();
        Intent intent = shadowOf(activity).getNextStartedActivity();
        ShadowIntent sIntent = shadowOf(intent);
        assertThat(sIntent.getIntentClass()).isEqualTo(LoginActivity.class);
    }

    @Test
    public void checkActivityResult(){
        ListFormActivity activity = Robolectric.setupActivity(ListFormActivity.class);
        activity.isi();
        shadowOf(activity).receiveResult(
                new Intent(activity, FormActivity.class),
                Activity.RESULT_OK,
                new Intent());

        assertEquals(true, activity.getRv_adapter().getItemCount() == 1);
    }

    public void deleteCard(){
        ListFormActivity activity = Robolectric.setupActivity(ListFormActivity.class);
        activity.isi();
        shadowOf(activity).receiveResult(
                new Intent(activity, FormActivity.class),
                Activity.RESULT_OK,
                new Intent());

        activity.getRv_adapter().deleteCard(0);
        assertEquals(true, activity.getRv_adapter().getItemCount() == 0);
    }

    @Test
    public void searchTest(){
        ListFormActivity activity = Robolectric.setupActivity(ListFormActivity.class);
        activity.isi();
        shadowOf(activity).receiveResult(
                new Intent(activity, FormActivity.class),
                Activity.RESULT_OK,
                new Intent());
        assertEquals(activity.getRv_adapter().searchIdxCardFromLocalId(0), 0);
    }

    @Test public void updateModel(){
        ListFormActivity activity = Robolectric.setupActivity(ListFormActivity.class);
        activity.getRv_adapter().add(new ListFormCardModel(0,0,0,"title","1.0"));
        activity.getRv_adapter().updateCardModel(0, new ListFormCardModel(1,0,0,"title","1.0"));
        assertEquals(activity.getRv_adapter().getCardModel(0).getLocalId(), 1);
    }
}
