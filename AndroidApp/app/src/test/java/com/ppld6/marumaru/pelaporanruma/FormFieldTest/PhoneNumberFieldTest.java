package com.ppld6.marumaru.pelaporanruma.FormFieldTest;

import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ppld6.marumaru.pelaporanruma.BuildConfig;
import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.NumberField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.PhoneNumberField;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by ricky on 06/04/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class PhoneNumberFieldTest {
    private FormActivity formActivity;
    private String errorCode;
    private String expErrorCode;

    private int VALID_ID;
    private final String VALID_LABEL        = "No. HP";
    private final String VALID_DESCRIPTION  = "No. HP KA yang signup";
    private final String VALID_PLACEHOLDER  = "1";
    private final boolean VALID_REQUIRED    = true;
    private final boolean VALID_REQUIRED2   = false;
    private final int VALID_MIN             = 0;
    private final int VALID_MAX             = 1000000;

    private final String VALID_NAME         = "Edison";
    private final String EMPTY_NAME         = "";

    @Before
    public void setup() {
        this.formActivity = Robolectric.setupActivity(FormActivity.class);
        this.VALID_ID     = View.generateViewId();
    }

    /*@Test
    public void render_WithLabelDescPlaceholderNotRequired_Success() {
        FormField field = new PhoneNumberField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2);
        View view = field.render(formActivity);
        EditText editText = (EditText) view.findViewById(id);

        assertThat(editText.getHint()).isEqualTo("placeholder1");
    }*/

    @Test
    public void render_WithoutDescription_Success() {
        String EMPTY_DESCRIPTION = "";
        FormField field = new PhoneNumberField(VALID_ID, VALID_LABEL, EMPTY_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2);
        View view = field.render(formActivity);

        TextView description = (TextView) view.findViewWithTag(VALID_ID + "_description");
        assertThat(description).isNull();
    }

    @Test
    public void result_AlphabeticInput_Success() {
        FormField field = new PhoneNumberField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2);
        View view = field.render(formActivity);

        // Fill EditText
        EditText editText = (EditText) view.findViewById(VALID_ID);
        editText.setText("aBdDef");
        assertThat(editText.getText().toString()).isEqualTo("08");
    }

    @Test
    public void result_AlphanumericInput_Success() {
        FormField field = new PhoneNumberField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2);
        View view = field.render(formActivity);

        // Fill EditText
        EditText editText = (EditText) view.findViewById(VALID_ID);
        editText.setText("1a2B3d4D5ef");
        assertThat(editText.getText().toString()).isEqualTo("08");
    }

    @Test
    public void result_NumericInput_Success() {
        FormField field = new PhoneNumberField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2);
        View view = field.render(formActivity);

        // Fill EditText
        EditText editText = (EditText) view.findViewById(VALID_ID);
        editText.setText("1234567890");
        assertThat(editText.getText().toString()).isEqualTo("08");
    }

    @Test
    public void validate_PhoneRequired_Success() {
        FormField field = new PhoneNumberField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED);
        View view = field.render(formActivity);

        EditText editText = (EditText) view.findViewById(VALID_ID);

        editText.setText("0823456789");
        assertThat(field.validate()).isTrue();

        editText.setText("08234567891");
        assertThat(field.validate()).isTrue();

        editText.setText("082345678912");
        assertThat(field.validate()).isTrue();

        editText.setText("0823456789123");
        assertThat(field.validate()).isTrue();
    }

    @Test
    public void validate_PhoneRequired_Fail() {
        FormField pf = new PhoneNumberField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED);
        View view = pf.render(formActivity);

        EditText editText = (EditText) view.findViewById(VALID_ID);

        /* Empty value */
        assertThat(pf.validate()).isFalse();
        expErrorCode = " (" + pf.getLabel().charAt(0) + pf.getId() + 0 + 1 + ")";
        errorCode = pf.getErrorCode();
        assertEquals(expErrorCode,errorCode);

        expErrorCode = " (" + pf.getLabel().charAt(0) + pf.getId() + 0 + 3 + ")";
        /* Contain Alphabet */
        editText.setText("01928ab103");
        assertThat(pf.validate()).isFalse();
        errorCode = pf.getErrorCode();
        //assertEquals(expErrorCode, errorCode);

        /* Contain Symbols */
        editText.setText("0124-235");
        assertThat(pf.validate()).isFalse();
        errorCode = pf.getErrorCode();
        //assertEquals(expErrorCode, errorCode);

        /* Too long */
        editText.setText("12345678901234");
        assertThat(pf.validate()).isFalse();
        expErrorCode = " (" + pf.getLabel().charAt(0) + pf.getId() + 0 + 2 + ")";
        errorCode = pf.getErrorCode();
        //assertEquals(expErrorCode, errorCode);
    }

    @Test
    public void validate_PhoneNotRequired_Success() {
        FormField field = new PhoneNumberField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2);
        View view = field.render(formActivity);

        EditText editText = (EditText) view.findViewById(VALID_ID);

        /* Empty value */
        editText.setText("08");
        assertThat(field.validate()).isTrue();
    }
}
