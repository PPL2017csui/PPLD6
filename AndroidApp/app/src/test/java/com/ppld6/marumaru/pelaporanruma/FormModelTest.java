package com.ppld6.marumaru.pelaporanruma;

/**
 * Created by edison on 22/03/17.
 */
import com.ppld6.marumaru.pelaporanruma.form.model.FormInterface;
import com.ppld6.marumaru.pelaporanruma.form.model.FormModel;
import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LongTextField;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitParamsRunner.class)
public class FormModelTest {

	private final static String VALID_ID 				= "1";
	private final static String VALID_NAME 				= "Tambah Prospek";
	private final static long VALID_VERSION 			= 1;
	private final static long VALID_CREATED_AT 			= 1489773600;
	private final static long VALID_UPDATED_AT 			= 1489774600;
	private final static String VALID_CREATED_BY 		= "Tantra";
	private final static String VALID_SET_NAME 			= "Tambahkan Prospek";
	private final static long VALID_SET_VERSION 		= 2;
	private final static long VALID_SET_CREATED_AT 		= 1489783600;
	private final static String VALID_SET_CREATED_BY 	= "Edison";

    private static final Object[] getInvalidId() {
		return new String[][]{{""}, {null}};
	}

	private static final Object[] getInvalidNameOrCreatedBy() {
		return new String[][]{{""}, {"!sl%th!"} , {"!@#$%#\'123?"}};
	}

	private static final Object[] getInvalidVersion() {
		return new Double[][]{{-1.123}, {-100.2}, {-5.1231}};
	}

	private static final Object[] getInvalidCreatedOrUpdatedAt() {
		return new long[][]{ {-100}, {-8234}, {-3708} ,{0} , {-10} , {-2} };
	}

	@InjectMocks
	FormModel injectMockForm = new FormModel();

   @Before
	public void setUp() {
	}

	@After
	public void tearDown(){
	}

	@Test
	public void instanceOf_TemplateForm_ReturnTrue(){
		FormModel form = new FormModel();
		assertTrue(form instanceof FormInterface);
	}

	@Test
	public void defaultValue_TemplateForm_ReturnDefaultValue(){
		FormModel form = new FormModel();
		assertEquals("new test should return empty value as string", "" , form.getName());
		assertEquals("new test should return 0 as long", 0 , form.getVersion());
		assertEquals("new test should return empty value as string", 0 , form.getUpdatedAt());
		assertEquals("new test should return 0 as integer", 0 , form.countFields());
	}

	@Test
	public void constructorShouldSuccess() {
		FormModel template = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);
		assertEquals(template.getId(), VALID_ID);
		assertEquals(template.getName(), VALID_NAME);
		assertEquals(template.getVersion(), VALID_VERSION);
		assertEquals(template.getCreatedBy(), VALID_CREATED_BY);
		assertEquals(template.getCreatedAt(), VALID_CREATED_AT);
		assertEquals(template.getUpdatedAt(), VALID_UPDATED_AT);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "getInvalidId")
	public void constructorShouldThrowIAEForInvalidId(String invalidId) {
		FormModel template = new FormModel(invalidId, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "getInvalidNameOrCreatedBy")
	public void constructorShouldThrowIAEForInvalidNameOrCreatedBy(String invalidName , String invalidCreatedBy) {
		FormModel template = new FormModel(VALID_ID, invalidName , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);

		FormModel template2 = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,invalidCreatedBy , VALID_CREATED_AT);
		template2.setUpdatedAt(VALID_UPDATED_AT);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "getInvalidVersion")
	public void constructorShouldThrowIAEForInvalidVersion(long invalidVersion) {
		FormModel template = new FormModel(VALID_ID, VALID_NAME , invalidVersion ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);
	}

	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "getInvalidCreatedOrUpdatedAt")
	public void constructorShouldThrowIAEForInvalidCreatedAt(long invalidCreatedAt) {
		FormModel template = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , invalidCreatedAt);
		template.setUpdatedAt(VALID_UPDATED_AT);
	}


	@Test(expected = IllegalArgumentException.class)
	@Parameters(method = "getInvalidCreatedOrUpdatedAt")
	public void constructorShouldThrowIAEForInvalidUpdatedAt(long invalidUpdatedAt) {
		FormModel template2 = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template2.setUpdatedAt(invalidUpdatedAt);
	}

	@Test
	public void setName_WithParam_ReturnVoid() {
		FormModel template = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);
		template.setName(VALID_SET_NAME);
		assertEquals(template.getName(), VALID_SET_NAME);
	}

	@Test
	public void setVersion_WithParam_ReturnVoid() {
		FormModel template = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);
		template.setVersion(VALID_SET_VERSION);
		assertEquals(template.getVersion(), VALID_SET_VERSION);
	}

	@Test
	public void setCreatedAt_WithParam_ReturnVoid() {
		FormModel template = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);
		template.setCreatedAt(VALID_SET_CREATED_AT);
		assertEquals(template.getCreatedAt(), VALID_SET_CREATED_AT);
	}

	@Test
	public void setCreatedBy_WithParam_ReturnVoid() {
		FormModel template = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.setUpdatedAt(VALID_UPDATED_AT);
		template.setCreatedBy(VALID_SET_CREATED_BY);
		assertEquals(template.getCreatedBy(), VALID_SET_CREATED_BY);
	}

	@Test
	public void validValue_AddField_ReturnSuccess(){
		FormField mockField = mock(FormField.class);
		when(mockField.getId()).thenReturn(1);
		FormModel template = new FormModel(VALID_ID, VALID_NAME , VALID_VERSION ,VALID_CREATED_BY , VALID_CREATED_AT);
		template.addField(mockField);
		assertEquals(mockField.getId(), template.listAllFields().get(0).getId());
	}

	@Test
	public void validId_GetField_ReturnSuccess(){
		FormField mockField = mock(FormField.class);
		when(mockField.getId()).thenReturn(1);
		injectMockForm.addField(mockField);
		assertEquals(injectMockForm.getField(1).getId(), 1);
	}

    @Test(expected = NullPointerException.class)
    public void invalidId_GetField_ReturnFail(){
        FormField mockField = mock(FormField.class);
        when(mockField.getId()).thenReturn(1);
        injectMockForm.addField(mockField);
    }

    @Test
    public void validVaue_UpdateField_ReturnSuccess(){
        FormField mockField = mock(FormField.class);
        FormField mockUpdatedField = mock(FormField.class);
        when(mockField.getId()).thenReturn(1);
        when(mockUpdatedField.getId()).thenReturn(2);
        injectMockForm.addField(mockField);
        injectMockForm.updateField(1, mockUpdatedField);
        assertEquals(injectMockForm.getField(2).getId(), 2);
    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void invalidValue_UpdateField_ReturnFail(){
        FormField mockUpdatedField = mock(FormField.class);

        when(mockUpdatedField.getId()).thenReturn(2);

        injectMockForm.updateField(1, mockUpdatedField);
    }

    @Test
    public void DeleteField_ReturnSuccess(){
        FormField mockField = mock(FormField.class);

        when(mockField.getId()).thenReturn(1);

        injectMockForm.addField(mockField);

        assertEquals(injectMockForm.countFields(), 1);
        injectMockForm.deleteField(1);
        assertEquals(injectMockForm.countFields(), 0);

    }

    @Test(expected = IndexOutOfBoundsException.class)
    public void invalidValue_DeleteField_ReturnFail(){
        injectMockForm.deleteField(1);
    }
}