package com.ppld6.marumaru.pelaporanruma.FormFieldTest;

import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ppld6.marumaru.pelaporanruma.BuildConfig;
import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LocationField;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by jefly on 05/04/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LocationFieldTest {

    FormActivity activity = Robolectric.setupActivity(FormActivity.class);

    private final int VALID_ID              = 1;
    private final String VALID_LABEL        = "Lokasi";
    private final String VALID_DESCRIPTION  = "Lokasi tempat anda melakukan kunjungan";
    private final String EMPTY_DESCRIPTION  = "";
    private final String VALID_PLACEHOLDER  = "Masukan Lokasi";
    private final String VALID_LOCATION     = "Jakarta";
    private final String EMPTY_LOCATION     = "";
    private final boolean VALID_REQUIRED    = true;
    private final boolean VALID_REQUIRED2   = false;

    @Test
    public void validValue_ResultTest_ReturnSuccess(){
        LocationField lf = new LocationField(VALID_ID,VALID_LABEL,EMPTY_DESCRIPTION,VALID_PLACEHOLDER, VALID_REQUIRED);
        EditText et = new EditText(activity);
        et.setText(VALID_LOCATION);
        TextInputLayout til_lf = new TextInputLayout(activity);
        til_lf.addView(et);
        lf.setView(til_lf);
        assertThat(lf.result()).isEqualTo(VALID_LOCATION);
    }

    @Test
    public void validValue_ValidateTest_ReturnSuccess(){
        LocationField lf = new LocationField(VALID_ID,VALID_LABEL,EMPTY_DESCRIPTION,VALID_PLACEHOLDER, VALID_REQUIRED);
        EditText et = new EditText(activity);
        et.setText(VALID_LOCATION);
        TextInputLayout til_lf = new TextInputLayout(activity);
        til_lf.addView(et);
        lf.setView(til_lf);
        assertThat(lf.validate()).isEqualTo(true);
    }

    @Test
    public void validValue_ValidateTest_ReturnSuccess2(){
        LocationField lf = new LocationField(VALID_ID,VALID_LABEL,EMPTY_DESCRIPTION,VALID_PLACEHOLDER, VALID_REQUIRED2);
        EditText et = new EditText(activity);
        et.setText(EMPTY_LOCATION);
        lf.setView(et);
        assertThat(lf.validate()).isEqualTo(true);
    }

    @Test
    public void emptyValue_ValidateTest_ReturnFail(){
        LocationField lf = new LocationField(VALID_ID,VALID_LABEL,EMPTY_DESCRIPTION,VALID_PLACEHOLDER, VALID_REQUIRED);
        EditText et = new EditText(activity);
        et.setText(VALID_LOCATION);
        TextInputLayout til_lf = new TextInputLayout(activity);
        til_lf.addView(et);
        lf.setView(til_lf);
        assertThat(lf.validate()).isEqualTo(true);
    }

    @Test
    public void validValue_RenderTest_ReturnSuccess(){
        LocationField lf = new LocationField(1,"lokasi","asd","Masukan Lokasi", true);
        assertThat(((LinearLayout)lf.render(activity)).getChildCount() == 2).isEqualTo(true);
    }

    @Test
    public void invalidValue_RenderTest_ReturnFail(){
        LocationField lf = new LocationField(VALID_ID,VALID_LABEL,EMPTY_DESCRIPTION,VALID_PLACEHOLDER, VALID_REQUIRED);
        assertThat(((LinearLayout)lf.render(activity)).getChildCount() == 3).isEqualTo(false);
    }
}
