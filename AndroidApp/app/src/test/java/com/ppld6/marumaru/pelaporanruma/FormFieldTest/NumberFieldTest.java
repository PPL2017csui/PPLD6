package com.ppld6.marumaru.pelaporanruma.FormFieldTest;

import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.ppld6.marumaru.pelaporanruma.BuildConfig;
import com.ppld6.marumaru.pelaporanruma.R;
import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LongTextField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.NumberField;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by ricky on 06/04/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class NumberFieldTest {
    FormActivity activity ;
    String errorCode;
    String expErrorCode;

    @Before
    public void setup(){
        this.activity = Robolectric.setupActivity(FormActivity.class);
    }


  /*  @Test
    public void render_WithLabelDescPlaceholderNotRequired_Success() {
        int id = View.generateViewId();
        EditText editText = new EditText(activity);
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true);
        TextInputLayout til_nf = new TextInputLayout(activity);
        til_nf.addView(editText);
        nf.setView(til_nf);

        assertThat(editText.getHint()).isEqualTo("placeholder1");
    }*/

  /*  @Test
    public void render_WithoutDescription_Success() {
        int id = View.generateViewId();
        EditText editText = new EditText(activity);
        NumberField nf = new NumberField(id, "label1", "", "placeholder1", false);
        TextInputLayout til_nf = new TextInputLayout(activity);
        til_nf.addView(editText);
        nf.setView(til_nf);

        TextView description = (TextView) view.findViewWithTag(VALID_ID + "_description");
        assertThat(description).isNull();
    }*/

    @Test
    public void result_AlphabeticInput_Success() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);
        editText.setText("aBdDef");
        assertThat(editText.getText().toString()).isEqualTo("aBdDef");
    }

    @Test
    public void result_AlphanumericInput_Success() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);
        editText.setText("1a2B3d4D5ef");
        assertThat(editText.getText().toString()).isEqualTo("1a2B3d4D5ef");
    }

    @Test
    public void result_NumericInput_Success() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);

        editText.setText("1234567890");
        assertThat(editText.getText().toString()).isEqualTo("1234567890");
    }

    @Test
    public void validate_NumberRequiredDefaultMinMax_Success() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);

        expErrorCode = " (" + nf.getLabel().charAt(0) + nf.getId() + 0 + 3 + ")";
        /* Minimum value */
        editText.setText("-2147483648");
        assertThat(nf.validate()).isTrue();

        /* Max value */
        editText.setText("2147483647");
        assertThat(nf.validate()).isTrue();

    }

    @Test
    public void validate_InputNotANumber_Fail(){
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);

        /* Value String */
        editText.setText("aaaaaaaaa");
        assertThat(nf.validate()).isFalse();
        expErrorCode = " (" + nf.getLabel().charAt(0) + nf.getId() + 0 + 2 + ")";
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);
    }
    @Test
    public void validate_NumberRequiredDefaultMinMax_Fail() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);



        /* Value Empty */
        editText.setText("");
        assertThat(nf.validate()).isFalse();
        expErrorCode = " (" + nf.getLabel().charAt(0) + nf.getId() + 0 + 1 + ")";
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        expErrorCode = " (" + nf.getLabel().charAt(0) + nf.getId() + 0 + 3 + ")";
        /* Min value */
        editText.setText("-2147483649");
        assertThat(nf.validate()).isFalse();
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        /* Min value */
        editText.setText("-4000000000");
        assertThat(nf.validate()).isFalse();
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        /* Max value */
        editText.setText("2147483648");
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        /* Max value */
        editText.setText("4000000000");
        assertThat(nf.validate()).isFalse();
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);
    }

    @Test
    public void validate_NumberNotRequiredDefaultMinMax_Success() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", false);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);

        /* Empty value */
        editText.setText("");
        assertThat(nf.validate()).isTrue();
    }

    @Test
    public void validate_NumberRequiredCustomMinMax_Success() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true,0,1000000);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);


        /* Min value */
        editText.setText("0");
        assertThat(nf.validate()).isTrue();

        /* Max value */
        editText.setText("1000000");
        assertThat(nf.validate()).isTrue();
    }

    @Test
    public void validate_NumberRequiredCustomMinMax_Fail() {
        int id = View.generateViewId();
        NumberField nf = new NumberField(id, "label1", "description1", "placeholder1", true,0,1000000);
        View view = nf.render(activity);
        EditText editText = (EditText) view.findViewById(id);

        /* Value Empty */
        editText.setText("");
        assertThat(nf.validate()).isFalse();
        expErrorCode = " (" + nf.getLabel().charAt(0) + nf.getId() + 0 + 1 + ")";
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        expErrorCode = " (" + nf.getLabel().charAt(0) + nf.getId() + 0 + 3 + ")";

        /* Min value */
        editText.setText("-1");
        assertThat(nf.validate()).isFalse();
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        /* Min value */
        editText.setText("-2147483648");
        assertThat(nf.validate()).isFalse();
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        /* Max value */
        editText.setText("1000000000");
        assertThat(nf.validate()).isFalse();
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);

        /* Max value */
        editText.setText("4000000000");
        assertThat(nf.validate()).isFalse();
        errorCode = nf.getErrorCode();
        assertEquals(expErrorCode, errorCode);
    }
}
