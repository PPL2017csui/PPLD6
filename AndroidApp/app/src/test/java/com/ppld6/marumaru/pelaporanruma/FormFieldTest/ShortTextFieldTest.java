package com.ppld6.marumaru.pelaporanruma.FormFieldTest;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.test.mock.MockContext;
import android.text.Layout;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ppld6.marumaru.pelaporanruma.BuildConfig;
import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.form.model.field.ShortTextField;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import static org.assertj.core.api.Java6Assertions.assertThat;
import junitparams.JUnitParamsRunner;

import static junit.framework.Assert.assertEquals;

/**
 * Created by kelvin on 05/04/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class ShortTextFieldTest {
    FormActivity activity = Robolectric.setupActivity(FormActivity.class);
    String expErrorCode;
    String errorCode;
    @Test
    public void validValue_ResultTest_ReturnSuccess(){
        String nama = "Edison";
        EditText et = new EditText(activity);
        et.setText(nama);
        ShortTextField stf = new ShortTextField(1,"nama","","Masukan Nama", true, 30);
        TextInputLayout til_stf = new TextInputLayout(activity);
        til_stf.addView(et);
        stf.setView(til_stf);
        assertThat((String)stf.result()).isEqualTo(nama);
    }

    @Test
    public void validValue_ValidationTest_ReturnSuccess(){
        String nama = "Edison";
        EditText et = new EditText(activity);
        et.setText(nama);
        ShortTextField stf = new ShortTextField(1,"nama","","Masukan Nama", true, 30);
        TextInputLayout til_stf = new TextInputLayout(activity);
        til_stf.addView(et);
        stf.setView(til_stf);
        assertThat(stf.validate()).isEqualTo(true);
    }

    @Test
    public void invalidValue_ValidationTest_ReturnFail(){
        String nama = "Edison";
        EditText et = new EditText(activity);
        et.setText(nama);
        ShortTextField stf = new ShortTextField(1,"nama","","Masukan Nama", true, 3);
        TextInputLayout til_stf = new TextInputLayout(activity);
        til_stf.addView(et);
        stf.setView(til_stf);
        assertThat(stf.validate()).isEqualTo(false);

    }

    @Test
    public void validValue_ValidationTest_ReturnSuccess2(){
        String nama = "";
        EditText et = new EditText(activity);
        et.setText(nama);
        ShortTextField stf = new ShortTextField(1,"nama","","Masukan Nama", false, 30);
        TextInputLayout til_stf = new TextInputLayout(activity);
        til_stf.addView(et);
        stf.setView(til_stf);
        assertThat(stf.validate()).isEqualTo(true);
    }

    @Test
    public void emptyValue_ValidationTest_ReturnFail(){
        String nama = "";
        EditText et = new EditText(activity);
        et.setText(nama);
        ShortTextField stf = new ShortTextField(1,"nama","","Masukan Nama", true, 3);
        TextInputLayout til_stf = new TextInputLayout(activity);
        til_stf.addView(et);
        stf.setView(til_stf);
        assertThat(stf.validate()).isEqualTo(false);
        errorCode = stf.getErrorCode();
        expErrorCode = " (" + stf.getLabel().charAt(0) + stf.getId() + 0 + 1 + ")";
        assertEquals(expErrorCode,errorCode);
    }

    @Test
    public void exceedMaxLength_ValidationTest_ReturnFail(){
        String nama = "aaaaaaaaaaaaa";
        EditText et = new EditText(activity);
        et.setText(nama);
        ShortTextField stf = new ShortTextField(1,"nama","","Masukan Nama", true, 3);
        TextInputLayout til_stf = new TextInputLayout(activity);
        til_stf.addView(et);
        stf.setView(til_stf);
        assertThat(stf.validate()).isEqualTo(false);
        errorCode = stf.getErrorCode();
        expErrorCode = " (" + stf.getLabel().charAt(0) + stf.getId() + 0 + 2 + ")";
        assertEquals(expErrorCode,errorCode);
    }

    @Test
    public void validValue_RenderTest_ReturnSuccess(){
        ShortTextField stf = new ShortTextField(1,"nama","drskripsi","Masukan Nama", true, 2);
        assertThat(((LinearLayout)stf.render(activity)).getChildCount()==2).isEqualTo(true);
    }

    @Test
    public void invalidValue_RenderTest_ReturnFail(){
        ShortTextField stf = new ShortTextField(1,"nama","","Masukan Nama", true, 4);
        assertThat(((LinearLayout)stf.render(activity)).getChildCount()==4).isEqualTo(false);
    }
}
