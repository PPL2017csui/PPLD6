package com.ppld6.marumaru.pelaporanruma;

import android.content.Intent;
import android.os.Handler;

import com.ppld6.marumaru.pelaporanruma.login.LoginActivity;
import com.ppld6.marumaru.pelaporanruma.splash.SplashActivity;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowIntent;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by ricky on 29/03/17.
 */


@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class SplashScreenTest {
    @Test
    public void runTest(){
        final SplashActivity activity = Robolectric.setupActivity(SplashActivity.class);
        activity.run();
        // code from http://www.androidhive.info/2013/07/how-to-implement-android-splash-screen-2/
        new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                Intent intent = shadowOf(activity).getNextStartedActivity();
                ShadowIntent sIntent = shadowOf(intent);
                assertThat(LoginActivity.class).isEqualTo(sIntent.getIntentClass());

            }
        }, 5000);

    }
}
