package com.ppld6.marumaru.pelaporanruma.FormFieldTest;

import android.content.Context;
import android.support.design.widget.TextInputLayout;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.ppld6.marumaru.pelaporanruma.BuildConfig;
import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LongTextField;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.assertj.core.api.Java6Assertions.assertThat;

/**
 * Created by edison on 06/04/17.
 */
@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class LongTextFieldTest {

    FormActivity activity = Robolectric.setupActivity(FormActivity.class);

    private final int VALID_ID              = 1;
    private final String VALID_LABEL        = "Nama";
    private final String EMPTY_DESCRIPTION  = "";
    private final String VALID_DESCRIPTION  = "Nama Anda";
    private final String VALID_PLACEHOLDER  = "Tantra";
    private final boolean VALID_REQUIRED    = true;
    private final boolean VALID_REQUIRED2   = false;
    private final int VALID_MAX_LENGTH      =  30;
    private final int VALID_MAX_LENGTH2     =  3;
    private final String VALID_NAME         = "Edison";
    private final String EMPTY_NAME         = "";

    @Test
    public void validValue_Validate_ReturnTrue(){
        LongTextField ltf = new LongTextField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED, VALID_MAX_LENGTH);
        TextInputLayout til_ltf = new TextInputLayout(activity);
        EditText et = new EditText(activity);
        et.setText(VALID_NAME);
        til_ltf.addView(et);
        ltf.setView(til_ltf);
        assertEquals((String)ltf.result(), VALID_NAME);
        assertTrue(ltf.validate());
    }

    @Test
    public void invalidValueMaxLength_Validate_ReturnFalse(){
        LongTextField ltf = new LongTextField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED, VALID_MAX_LENGTH2);
        TextInputLayout til_ltf = new TextInputLayout(activity);
        EditText et = new EditText(activity);
        et.setText("asdfasdf");
        til_ltf.addView(et);
        ltf.setView(til_ltf);
        assertFalse(ltf.validate());
        String expErrorCode = " (" + ltf.getLabel().charAt(0) + ltf.getId() + 0 + 2 + ")";
        assertEquals(expErrorCode, ltf.getErrorCode());
    }

    @Test
    public void invalidValueRequired_Validate_ReturnTrue(){
        LongTextField ltf = new LongTextField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2, VALID_MAX_LENGTH2);
        EditText et = new EditText(activity);
        et.setText(VALID_NAME);
        ltf.setView(et);
        assertTrue(ltf.validate());
    }

    @Test
    public void emptyValue_Validate_ReturnFalse(){
        LongTextField ltf = new LongTextField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED, VALID_MAX_LENGTH2);
        TextInputLayout til_ltf = new TextInputLayout(activity);
        EditText et = new EditText(activity);
        til_ltf.addView(et);
        ltf.setView(til_ltf);
        assertFalse(ltf.validate());
        String expErrorCode  = " (" + ltf.getLabel().charAt(0) + ltf.getId() + 0 + 1 + ")";
        assertEquals(expErrorCode, ltf.getErrorCode());
    }

    @Test
    public void validValueRequired_Render_ReturnSuccess(){
        LongTextField ltf = new LongTextField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED, VALID_MAX_LENGTH2);
        assertThat(((LinearLayout)ltf.render(activity)).getChildCount()==2).isEqualTo(true);
    }

    @Test
    public void validValueNotRequired_Render_ReturnSuccess(){
        LongTextField ltf = new LongTextField(VALID_ID, VALID_LABEL, VALID_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED2, VALID_MAX_LENGTH2);
        assertThat(((LinearLayout)ltf.render(activity)).getChildCount()==2).isEqualTo(true);
    }

    @Test
    public void invalidValue_RenderTest_ReturnFail(){
        LongTextField ltf = new LongTextField(VALID_ID, VALID_LABEL, EMPTY_DESCRIPTION, VALID_PLACEHOLDER, VALID_REQUIRED, VALID_MAX_LENGTH2);
        assertThat(((LinearLayout)ltf.render(activity)).getChildCount()==3).isEqualTo(false);
    }

}
