package com.ppld6.marumaru.pelaporanruma;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;

import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.form.FormPresenterImpl;
import com.ppld6.marumaru.pelaporanruma.form.FormView;
import com.ppld6.marumaru.pelaporanruma.form.model.FormModel;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowConnectivityManager;
import org.robolectric.shadows.ShadowNetworkInfo;
import org.robolectric.shadows.ShadowToast;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by ricky on 29/03/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
public class FormActivityTest {

    private static final int VALID_ID = 1;
    private ConnectivityManager connectivityManager;
    private ShadowConnectivityManager shadowConnectivityManager;
    private ShadowNetworkInfo shadowOfActiveNetworkInfo;
    private Context context;

    @Before
    public void setUp(){
        connectivityManager = (ConnectivityManager) RuntimeEnvironment.application.getSystemService(Context.CONNECTIVITY_SERVICE);
        shadowConnectivityManager = shadowOf(connectivityManager);
        shadowOfActiveNetworkInfo = shadowOf(connectivityManager.getActiveNetworkInfo());
    }

    @Test
    public void validValue_OnCreate_ShowToast(){
        FormActivity activity = Robolectric.setupActivity(FormActivity.class);
        String halo = "Halo";
        activity.showToast(halo);
        assertThat(ShadowToast.getTextOfLatestToast()).isEqualTo(halo);
    }

    @Test
    public void validValue_RenderForm_ReturnSuccess(){
        FormActivity activity   = Robolectric.setupActivity(FormActivity.class);
        FormModel formField     = new FormModel();
        activity.renderForm(formField);
    }

    @Test(expected = NullPointerException.class)
    public void validValue_SubmitForm_ReturnSuccess(){
        context                          = mock(Context.class);
        FormView mockFormView            = mock(FormView.class);
        FormPresenterImpl injectMockForm = new FormPresenterImpl(VALID_ID,context, mockFormView);
        injectMockForm.submitForm();
    }

    @Test(expected = NullPointerException.class)
    public void validValuePresenter_GetImageUri_ReturnURi(){
        context                          = mock(Context.class);
        FormView mockFormView            = mock(FormView.class);
        Bitmap mockBitmap                = mock(Bitmap.class);
        FormPresenterImpl injectMockForm = new FormPresenterImpl(VALID_ID,context, mockFormView);
        injectMockForm.getImageUri(mockBitmap);
    }

/*
    @Test
    public void changeLayout_NotConnectedAndLocationFieldExist_ShouldDisconnect(){
        NetworkInfo networkInfo =  ShadowNetworkInfo.newInstance(NetworkInfo.DetailedState.DISCONNECTED,
                ConnectivityManager.TYPE_WIFI, 0, true, false);
        shadowConnectivityManager.setActiveNetworkInfo(networkInfo);
        FormActivity activity   = Robolectric.setupActivity(FormActivity.class);
        FormModel formField     = new FormModel();
        LocationField mockLocation = mock(LocationField.class);
        formField.addField(mockLocation);
        activity.renderForm(formField);
    }

    @Test
    public void changeLayout_NotConnectedAndLocationFieldNotExist_ShouldDisconnect(){
        NetworkInfo networkInfo =  ShadowNetworkInfo.newInstance(NetworkInfo.DetailedState.DISCONNECTED,
                ConnectivityManager.TYPE_WIFI, 0, true, false);
        shadowConnectivityManager.setActiveNetworkInfo(networkInfo);
        FormActivity activity   = Robolectric.setupActivity(FormActivity.class);
        FormModel formField     = new FormModel();
        LocationField mockLocation = mock(LocationField.class);
        formField.addField(mockLocation);
        activity.renderForm(formField);
    }

    @Test
    public void onNetworkChange_NotConnected_ShouldToastError(){
        NetworkInfo networkInfo =  ShadowNetworkInfo.newInstance(NetworkInfo.DetailedState.DISCONNECTED,
                ConnectivityManager.TYPE_WIFI, 0, true, false);
        shadowConnectivityManager.setActiveNetworkInfo(networkInfo);
        FormActivity activity   = Robolectric.setupActivity(FormActivity.class);
        FormModel formField     = new FormModel();
        activity.onNetworkConnectionChanged(false);
        activity.renderForm(formField);
        assertThat(ShadowToast.getTextOfLatestToast()).isEqualTo("Koneksi Terputus");
    }
    */
}
