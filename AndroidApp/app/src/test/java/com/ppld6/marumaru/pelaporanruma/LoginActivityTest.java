package com.ppld6.marumaru.pelaporanruma;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ppld6.marumaru.pelaporanruma.listForm.ListFormActivity;
import com.ppld6.marumaru.pelaporanruma.login.LoginActivity;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowAlertDialog;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowToast;


import javassist.bytecode.analysis.Executor;

import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doAnswer;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.robolectric.Shadows.shadowOf;

/**
 * Created by ricky on 29/03/17.
 */

@RunWith(RobolectricTestRunner.class)
@Config(constants = BuildConfig.class)
@PowerMockIgnore({ "org.mockito.*", "org.robolectric.*", "android.*" })
@PrepareForTest({ FirebaseDatabase.class})
public class LoginActivityTest {

    private DatabaseReference mockedDatabaseReference;

    @Before
    public void before(){
        FirebaseDatabase mockedFirebaseDatabase = mock(FirebaseDatabase.class);
        when(mockedFirebaseDatabase.getReference()).thenReturn(mockedDatabaseReference);

        PowerMockito.mockStatic(FirebaseDatabase.class);
        when(FirebaseDatabase.getInstance()).thenReturn(mockedFirebaseDatabase);
        when(mockedDatabaseReference.child(anyString())).thenReturn(mockedDatabaseReference);
        doAnswer(new Answer<Void>() {
            @Override
            public Void answer(InvocationOnMock invocation) throws Throwable {
                ValueEventListener valueEventListener = (ValueEventListener) invocation.getArguments()[0];

                DataSnapshot mockedDataSnapshot = Mockito.mock(DataSnapshot.class);
                //when(mockedDataSnapshot.getValue(User.class)).thenReturn(testOrMockedUser)

                valueEventListener.onDataChange(mockedDataSnapshot);
                //valueEventListener.onCancelled(...);

                return null;
            }
        }).when(mockedDatabaseReference).addListenerForSingleValueEvent(any(ValueEventListener.class));
        new LoginActivity();
    }

    @Test
    public void onCreate_screen_shouldShowSignInText () {
        LoginActivity activity = Robolectric.setupActivity(LoginActivity.class);
        Button btn = (Button) activity.findViewById(R.id.btn_login);
        assertThat(btn.getText()).isEqualTo("Sign In");
    }

    @Test
    public void attemptLogin_onSignInBtnClick_shouldShowProgressDialog () {

        LoginActivity activity = Robolectric.setupActivity(LoginActivity.class);
        EditText et_phone = (EditText) activity.findViewById(R.id.et_phone_number);
        EditText et_passwd = (EditText) activity.findViewById(R.id.et_password);
        et_phone.setText("0000");
        et_passwd.setText("123456");


//        activity.findViewById(R.id.btn_login).performClick();
//        AlertDialog alert = ShadowAlertDialog.getLatestAlertDialog();
//        ShadowAlertDialog sAlert = shadowOf(alert);
//
//        assertThat(sAlert.getTitle().toString())
//                .isEqualTo("Logged In");
    }

    @Test
    public void attemptLogin_onSignInBtnClick_failed_shouldShowToast () {

        LoginActivity activity = Robolectric.setupActivity(LoginActivity.class);
        EditText et_phone = (EditText) activity.findViewById(R.id.et_phone_number);
        EditText et_passwd = (EditText) activity.findViewById(R.id.et_password);
        et_phone.setText("0000");
        et_passwd.setText("12345678");
        activity.showError("Login Error");

        assertThat(ShadowToast.getTextOfLatestToast()).isEqualTo("Login Error");
    }

    @Test
    public void attempLogin_onSignInBtnClick_shouldStartFormActivity(){
        LoginActivity activity = Robolectric.setupActivity(LoginActivity.class);
        activity.navigateToHome();

        Intent intent = shadowOf(activity).getNextStartedActivity();
        ShadowIntent sIntent = shadowOf(intent);
        assertThat(sIntent.getIntentClass()).isEqualTo(ListFormActivity.class);
    }
}
