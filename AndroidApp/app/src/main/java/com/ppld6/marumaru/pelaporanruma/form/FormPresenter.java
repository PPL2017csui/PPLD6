package com.ppld6.marumaru.pelaporanruma.form;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by jefly on 25/03/17.
 */

public interface FormPresenter {
    public void generateForm(boolean enabled);
    public void loadAnswerData();
    public void submitForm();
    public Uri getImageUri(Bitmap photo);
    public void saveData(int status);
    public boolean isLoaded();
    public void clearAllUploadTask();
}
