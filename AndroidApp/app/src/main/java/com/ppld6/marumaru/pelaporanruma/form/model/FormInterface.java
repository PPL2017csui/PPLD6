package com.ppld6.marumaru.pelaporanruma.form.model;

import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;

import java.util.ArrayList;

/**
 * Created by edison on 22/03/17.
 */

public interface FormInterface {
	public String getId();
	public String getName();
	public void setName(String newName);
	public long getVersion();
	public void setVersion(long newVersion);
	public long getCreatedAt();
	public void setCreatedAt(long createdDate);
	public String getCreatedBy();
	public void setCreatedBy(String createdBy);
	public long getUpdatedAt();
	public void setUpdatedAt(long updated);
	public ArrayList<FormField> listAllFields();
	public int countFields();
	public void addField(FormField field);
	public FormField getField(int idField);
	public void updateField(int idField, FormField updatedField);
	public void deleteField(int idField);
}
