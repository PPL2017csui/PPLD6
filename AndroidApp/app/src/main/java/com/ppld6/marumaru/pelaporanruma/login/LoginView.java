package com.ppld6.marumaru.pelaporanruma.login;

/**
 * Created by jefly on 25/03/17.
 */

public interface LoginView {
    void showProgress();

    void hideProgress();

    void showError(String message);

    void navigateToHome();

    void clearPasswordField();

    void resetActivity();
}
