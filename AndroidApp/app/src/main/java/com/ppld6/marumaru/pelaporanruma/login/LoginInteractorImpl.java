package com.ppld6.marumaru.pelaporanruma.login;

import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by jefly on 25/03/17.
 */

public class LoginInteractorImpl implements LoginInteractor {
    FirebaseAuth auth = FirebaseAuth.getInstance();

    @Override
    public void login(String phoneNumber, String password, final OnLoginFinishedListener listener) {
        if(phoneNumber.trim().isEmpty() || password.isEmpty()){
            listener.onError("No Telp atau Password kosong L02");
            return;
        }

        auth.signInWithEmailAndPassword(phoneNumber, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users/"+uid);

                    ref.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if(dataSnapshot.child("active").getValue(Boolean.class)){
                                listener.onSuccess();
                            } else {
                                listener.onError("User tidak aktif L04");
                                FirebaseAuth.getInstance().signOut();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                } else {
                    listener.onError("Login gagal, salah username atau password L01");
                }
            }
        });
    }
}
