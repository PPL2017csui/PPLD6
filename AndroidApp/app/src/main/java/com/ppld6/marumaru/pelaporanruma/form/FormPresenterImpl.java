package com.ppld6.marumaru.pelaporanruma.form;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.widget.EditText;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LocationField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LongTextField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.NumberField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.PhoneNumberField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.PictureField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.ShortTextField;
import com.ppld6.marumaru.pelaporanruma.savedInstance.FilledData;
import com.ppld6.marumaru.pelaporanruma.utils.SharedPreferencesManager;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jefly on 27/03/17.
 */

public class FormPresenterImpl implements FormPresenter {
    /*ketentuan field type*/
    final int SHORT_TEXT_FIELD_TYPE = 1;
    final int LONG_TEXT_FIELD_TYPE = 2;
    final int NUMBER_FIELD_TYPE = 3;
    final int PHONE_NUMBER_FIELD_TYPE = 4;
    final int PICTURE_FIELD_TYPE = 5;
    final int LOCATION_FIELD_TYPE = 6;
    FormInteractor formInteractor;
    FormView formView;
    Context context;

    /*local id untuk recycler view*/
    int rv_id;

    /*menunjukkan jika form berhasil di load*/
    boolean loaded = false;

    public FormPresenterImpl(int rv_id, Context context, FormView formView){
        this.rv_id = rv_id;
        formInteractor = new FormInteractorImpl(context);
        this.formView = formView;
        this.context = context;
    }

    public boolean isLoaded(){
        return this.loaded;
    }

    /**
     * Membuat form dengan field-field dari database
     */

    @Override
    public void generateForm(final boolean enabled) {
        //menampilkan loading progress bar
        formView.showLoadingFormProgressBar();

        //definisi listener ketika sukses mengambil data form template dari database
        ValueEventListener listener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //jika belum di load sebelumnya
                if(!loaded) {
                    //iterasi seluruh template
                    for (DataSnapshot formTemplateSnapshot : dataSnapshot.getChildren()) {
                        Long activeForm = formTemplateSnapshot.child("active").getValue(Long.class);
                        //jika form aktif
                        if (activeForm != 0) {
                            //set form id
                            formInteractor.getFormModel().setId(formTemplateSnapshot.getKey());
                            formInteractor.getFormModel().setVersion(activeForm);
                            Log.d("form", formTemplateSnapshot.getKey());
                            //mendapatkan versi yang aktif
                            DataSnapshot newFormVersionSnapshot = formTemplateSnapshot.child(""+activeForm);
                            //set nama/judul form
                            formInteractor.getFormModel().setName(newFormVersionSnapshot.child("name").getValue(String.class));
                            //mengambil waktu form dibuat
                            formInteractor.getFormModel().setCreatedAt(newFormVersionSnapshot.child("created_at").getValue(Long.class));
                            //mengambil waktu form terakhir diupdate
                            formInteractor.getFormModel().setUpdatedAt(newFormVersionSnapshot.child("updated_at").getValue(Long.class));


                            int id = 1;
                            //iterasi field form
                            for (DataSnapshot formFieldsSnapshot : newFormVersionSnapshot.child("fields").getChildren()) {
                                //tipe field(gambar, shortText, dll)
                                int fieldType = formFieldsSnapshot.child("field_type").getValue(Integer.class);
                                //label
                                String label = formFieldsSnapshot.child("label").getValue(String.class);
                                //deskripsi
                                String description = formFieldsSnapshot.child("description").getValue(String.class);
                                //nama field
                                String name = formFieldsSnapshot.child("name").getValue(String.class);
                                //placeholder
                                String placeholder = formFieldsSnapshot.child("placeholder").getValue(String.class);
                                boolean required = formFieldsSnapshot.child("required").getValue(Boolean.class);

                                int order = formFieldsSnapshot.child("order").getValue(Integer.class);
                                //maxlength
                                int maxLength = 0;
                                switch (fieldType) {
                                    case LONG_TEXT_FIELD_TYPE:
                                        maxLength = formFieldsSnapshot.child("maxlength").getValue(Integer.class);
                                        formInteractor.addModel(new LongTextField(id, name, label, description, placeholder, required, maxLength, order, enabled));
                                        break;
                                    case SHORT_TEXT_FIELD_TYPE:
                                        maxLength = formFieldsSnapshot.child("maxlength").getValue(Integer.class);
                                        formInteractor.addModel(new ShortTextField(id, name, label, description, placeholder, required, maxLength, order, enabled));
                                        break;
                                    case PHONE_NUMBER_FIELD_TYPE:
                                        formInteractor.addModel(new PhoneNumberField(id, name, label, description, placeholder, required, order, enabled));
                                        break;
                                    case LOCATION_FIELD_TYPE:
                                        formInteractor.addModel(new LocationField(id, name, label, description, placeholder, required, order, enabled));
                                        break;
                                    case PICTURE_FIELD_TYPE:
                                        formInteractor.addModel(new PictureField(id, name, label, description, placeholder, required, order, enabled));
                                        break;
                                    case NUMBER_FIELD_TYPE:
                                        formInteractor.addModel(new NumberField(id, name, label, description, placeholder, required, order, enabled));
                                        break;
                                }
                                id++;
                            }
                            //tutup progressbar
                            formView.hideLoadingFormProgressBar();
                            //tampilkan form ke view
                            formView.renderForm(formInteractor.getFormModel());
                            //load data dari sharedpreferences
                            loadAnswerData();
                            //berhasil di load
                            loaded = true;
                        }
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Read Failed : ", databaseError.getMessage());
            }
        };
        //mengambil dari database dengan definisi listener diatas
        formInteractor.getFormModelFromFirebase(listener);
    }

    /**
     * Menyimpan data hasil isian
     * @param status
     */
    public void saveData(final int status){
        //mendapatkan keseluruhan field
        ArrayList<FormField> listField = formInteractor.getFormModel().listAllFields();
        //membuat objek filled data
        FilledData filledData = new FilledData(rv_id, formInteractor.getFormModel().getName());
        //preview text, text yang ditampilkan sebagaian pada saat menampilkan list form
        filledData.previewText="";
        //loop list field
        for (FormField field : listField) {
            //mengambil value hasil input
            String result = (String) field.result();
            filledData.answerMap.put(field.getId(), result);
            //mengecek field tidak kosong
            if(field instanceof PhoneNumberField && !result.equals("08") || !(field instanceof PhoneNumberField) && !result.isEmpty()){
                filledData.previewText+=field.getLabel()+" : "+field.result()+", ";
            }
        }
        //jika field kosong
        if(filledData.previewText.isEmpty()){
            filledData.previewText="kosong";
        } else{
            filledData.previewText=filledData.previewText.substring(0,filledData.previewText.length()-2);
        }
        //set status success atau disimpan
        filledData.status = status;
        //mengupdate waktu update
        filledData.lastEdit = System.currentTimeMillis();
        Gson gson = new Gson();
        //predefine type dari json
        Type type = new TypeToken<List<FilledData>>() {}.getType();
        //mendapatkan input jawaban terakhir
        String lastAnswer = SharedPreferencesManager.getFormAnswer(context);

        if (lastAnswer != null) {
            //jika blum ada jawaban sebelumnya
            if (lastAnswer.isEmpty()) {
                List<FilledData> listFilledData = new ArrayList<>();
                listFilledData.add(filledData);
                String json = gson.toJson(listFilledData, type);
                //simpan di sharedpreferences
                SharedPreferencesManager.storeFormAnswer(json, context);
            //jika sudah ada
            } else {
                List<FilledData> listFilledData = gson.fromJson(lastAnswer, type);
                boolean flag = false;
                //cari dengan id yang sama
                for (int i = 0; i < listFilledData.size(); i++) {
                    if (listFilledData.get(i).id == rv_id) {
                        listFilledData.set(i, filledData);
                        flag = true;
                    }
                }
                if (!flag) {
                    listFilledData.add(filledData);
                }
                String json = gson.toJson(listFilledData, type);
                //simpan di shared preferences
                SharedPreferencesManager.storeFormAnswer(json, context);
            }
        }
    }

    /**
     * Load jawaban yang tersimpan
     */
    public void loadAnswerData(){
        ArrayList<FormField> listFields = formInteractor.getFormModel().listAllFields();
        Gson gson = new Gson();
        Type type = new TypeToken<List<FilledData>>(){}.getType();
        String lastAnswer = SharedPreferencesManager.getFormAnswer(context);

        List<FilledData> listFilledData = null;
        if(lastAnswer != null) {
            //jika jawaban sebelumnya tidak kosong
            if (!lastAnswer.isEmpty()) {
                listFilledData = gson.fromJson(lastAnswer, type);
            }
        }
        if(listFilledData != null){
            //iterasi keseluruhan field, untuk mencari local id yang sama dengan local id yang ditampilkan
            for(FilledData filledData : listFilledData){
                //jika id local ditemukan
                if(rv_id == filledData.id){
                    //iterasi semua field
                    for(FormField formField : listFields){
                        // jika string maka langsung diisi ke edit text
                        if(formField instanceof LocationField || formField instanceof LongTextField || formField instanceof NumberField ||
                                formField instanceof PhoneNumberField || formField instanceof ShortTextField){
                            if(filledData.answerMap.containsKey(formField.getId())){
                                ((TextInputLayout)formField.getView()).getEditText().setText(filledData.answerMap.get(formField.getId()));
                            }
                        //jika picture, tampilkan di imageview
                        } else if(formField instanceof PictureField){
                            ((PictureField)formField).setPath(filledData.answerMap.get(formField.getId()));
                            ((PictureField)formField).updateImageView(context);
                        }
                    }
                }
            }
        }
    }

    public void removeData(int id){
        Gson gson = new Gson();
        Type type = new TypeToken<List<FilledData>>(){}.getType();
        String json = SharedPreferencesManager.getFormAnswer(context);
        List<FilledData> listFilledData = gson.fromJson(json,type);
        for(int i = 0; i<listFilledData.size(); i++){
            if(listFilledData.get(i).id == id){
                listFilledData.remove(i);
                break;
            }
        }
        json = gson.toJson(listFilledData, type);
        Log.d("delete", json);
        SharedPreferencesManager.storeFormAnswer(json, context);
    }

    @Override
    public void submitForm() {
        formView.showProgress();
        String errorField = formInteractor.submitFormFields(new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                formView.hideProgress();
                if(databaseError == null){
                    formView.showToast("Sukses Mengirim");
                    formView.setSuccessResult();
                } else {
                    formView.showToast("Gagal Mengirim, Internal error.");
                }
            }
        });
        if(errorField != null){
            formView.hideProgress();
            formView.showToast("Kesalahan dalam Pengisian Field "+errorField);
        }
    }

    /*@Override
    public void setFormFieldResult(int id, String key, String value) {
        ArrayList<FormFieldModel> formFields = formInteractor.getFormModel().listAllFields();
        for(int i = 0; i< formFields.size(); i++){
            if(id == formFields.get(i).getId()){
                formFields.get(i).addResult(key, value);
            }
        }
    }*/

    @Override
    public Uri getImageUri(Bitmap photo) {
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), photo, "Title", null);
        return Uri.parse(path);
    }

    public void clearAllUploadTask(){
        formInteractor.clearAllUploadTask();
    }
}
