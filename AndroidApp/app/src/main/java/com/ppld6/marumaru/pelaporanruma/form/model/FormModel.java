package com.ppld6.marumaru.pelaporanruma.form.model;

import android.util.Log;

import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;

import java.util.ArrayList;

/**
 * Created by jefly on 25/03/17.
 */

public class FormModel implements FormInterface{
    ArrayList<FormField> formFields;
    String id;
    String name;
    long version;
    long createdAt;
    String createdBy;
    long updatedAt;

    public FormModel(String id, String name, long version, String createdBy, long createdAt){

        if(name==null || createdBy == null){
            throw new NullPointerException();
        }
        if( (id == null || id.equals("")) || !name.matches("^[A-Za-z\\s]*$") || name.equals("") || version < 0 ||
                !createdBy.matches("^[A-Za-z\\s]*$") || createdBy.equals("")  || createdAt < 0 ) {
            throw new IllegalArgumentException();
        }

        this.id = id;
        this.name = name;
        this.version = version;
        this.createdAt = createdAt;
        this.createdBy = createdBy;
        this.updatedAt = 0;
        this.formFields = new ArrayList<>();
    }

    public FormModel(){
        this.id= "dummy_string";
        this.formFields = new ArrayList<>();
        this.name = "";
        this.version = 0;
        this.createdAt = 0;
        this.createdBy = "";
        this.updatedAt = 0;
    }

    @Override
    public String getId() {
        return this.id;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public void setName(String newName) {
        this.name = newName;
    }

    @Override
    public long getVersion() {
        return this.version;
    }

    @Override
    public void setVersion(long newVersion) {
        this.version = newVersion;
    }

    @Override
    public long getCreatedAt() {
        return this.createdAt;
    }

    @Override
    public void setCreatedAt(long createdDate) {
        this.createdAt = createdDate;
    }

    @Override
    public String getCreatedBy() {
        return this.createdBy;
    }

    @Override
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Override
    public long getUpdatedAt() {
        return this.updatedAt;
    }

    @Override
    public void setUpdatedAt(long updated) {
        this.updatedAt = updated;
    }

    @Override
    public ArrayList<FormField> listAllFields() {
        return this.formFields;
    }

    @Override
    public int countFields() {
        return this.formFields.size();
    }

    @Override
    public void addField(FormField field) {
        this.formFields.add(field);
    }

    @Override
    public FormField getField(int idField) {
        for(FormField formField : formFields){
            if(formField.getId() == idField) return formField;
        }
        return null;
    }

    @Override
    public void updateField(int idField, FormField updatedField) {
        if(formFields.isEmpty()){
            throw new IndexOutOfBoundsException();
        }
        for(int i = 0; i<formFields.size();i++){
            if(formFields.get(i).getId() == idField){
                formFields.set(i, updatedField);
                return;
            }
        }
    }

    @Override
    public void deleteField(int idField) {
        if(formFields.isEmpty()){
            throw new IndexOutOfBoundsException();
        }
        for(int i = 0; i<formFields.size();i++){
            if(formFields.get(i).getId() == idField){
                formFields.remove(i);
                return;
            }
        }
    }

    public void setId(String id){
        this.id = id;
    }
}
