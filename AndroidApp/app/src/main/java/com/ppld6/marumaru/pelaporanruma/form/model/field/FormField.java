package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.content.Context;
import android.view.View;

import java.text.Normalizer;
import java.util.logging.Logger;

/* Created by kelvin on 05/04/17.*/

public abstract class FormField implements Comparable<FormField> {
    protected int id;
    protected String name;
    protected String label;
    protected String description;
    protected String placeholder;
    protected String errorCode;
    protected View view;
    protected boolean required;
    protected int order;
    protected boolean enabled;

    public FormField(int id, String label, String description, String placeholder, boolean required, boolean enabled) {
        this.id = id;
        this.label = label;
        this.description = description;
        this.placeholder = placeholder;
        this.required = required;
        this.errorCode="";
        this.view = null;
        this.enabled = enabled;
    }

    public FormField(int id, String name, String label, String description, String placeholder, boolean required, int order, boolean enabled) {
        this.id = id;
        this.label = label;
        this.description = description;
        this.placeholder = placeholder;
        this.required = required;
        this.name = name;
        this.view = null;
        this.order = order;
        this.enabled = enabled;
    }

    public int getId() {
        return id;
    }
    public String getLabel(){return label;}
    public String getName(){return name;}
    public String getDescription(){return description;}
    public String getPlaceholder(){return placeholder;}
    public boolean getRequired(){return required;}
    public String getErrorCode(){return errorCode;}
    public View getView(){return view;}
    public abstract boolean validate();
    public abstract View render(Context context);
    public abstract Object result();
    public int compareTo(FormField field){
        if(this.order < field.order ){
            return -1;
        } else {
            return 1;
        }
    }
}
