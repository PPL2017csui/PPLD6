package com.ppld6.marumaru.pelaporanruma.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.firebase.auth.FirebaseAuth;

/**
 * Created by jefly on 13/05/17.
 */

public class SharedPreferencesManager {
    public static final String FORM_ANSWER_KEY = "marumaruformanswer";
    public static final String AUTO_INCREMENT_FORM_ID_KEY = "marumaruformidkey";
    public static final String CURRENT_DATE_KEY = "marumarucurrentdatekey";

    public static void storeFormAnswer(String str, Context context){
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(FORM_ANSWER_KEY+uid, str);
        editor.commit();
    }

    public static String getFormAnswer(Context context){
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedpreferences.getString(FORM_ANSWER_KEY+uid, "");
    }

    public static void formIdAutoIncrement(Context context){
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        int number = sharedpreferences.getInt(AUTO_INCREMENT_FORM_ID_KEY+uid, 0)+1;
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(AUTO_INCREMENT_FORM_ID_KEY+uid, number);
        editor.commit();
    }

    public static int currentFormId(Context context){
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedpreferences.getInt(AUTO_INCREMENT_FORM_ID_KEY+uid, 0);
    }

    public static String currentDate(Context context){
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedpreferences.getString(CURRENT_DATE_KEY, "");
    }

    public static void updateCurrentDate(Context context){
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(CURRENT_DATE_KEY, Utils.getDateStringByTimeMillis(System.currentTimeMillis()));
        editor.commit();
    }


    public static void clear(Context context){
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }
}
