package com.ppld6.marumaru.pelaporanruma.listForm;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.DrawableRes;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ppld6.marumaru.pelaporanruma.R;
import com.ppld6.marumaru.pelaporanruma.listForm.model.ListFormCardModel;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by jefly on 23/04/17.
 */

public class ListFormAdapter extends SelectableAdapter<ListFormAdapter.CardViewHolder>{
    //model dari card suatu laporan
    private ArrayList<ListFormCardModel> cardModelList;

    private ListFormView listFormView;
    private CardViewHolder.ClickListener clickListener;
    private Context context;
    public ListFormAdapter(ArrayList<ListFormCardModel> cardModelList, ListFormView listFormView, Context context){
        this.cardModelList      = cardModelList;
        this.listFormView       = listFormView;
        this.context = context;
    }

    public ListFormAdapter(ArrayList<ListFormCardModel> cardModelList, ListFormView listFormView,
                           CardViewHolder.ClickListener clickListener, Context context){
        this.cardModelList      = cardModelList;
        this.listFormView       = listFormView;
        this.clickListener      = clickListener;
        this.context = context;
    }


    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_form, parent, false);
        if(view == null){
            Log.d("listform", "null");
        }
        CardViewHolder myViewHolder = new CardViewHolder(view, clickListener);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(CardViewHolder holder, final int position) {
        Log.d("listform", position+"");
        TextView tv_title       = holder.tv_title;
        TextView tv_lastEdit    = holder.tv_lastEdit;
        TextView tv_preview     = holder.tv_preview;

        Drawable drawable_success = context.getResources().getDrawable(R.drawable.ic_check_black_24dp);
        ColorFilter drawable_successFilter = new LightingColorFilter(Color.BLACK, context.getResources().getColor(R.color.colorPrimary));
        drawable_success.setColorFilter(drawable_successFilter);

        Drawable drawable_draft = context.getResources().getDrawable(R.drawable.ic_save_black_24dp);
        ColorFilter drawable_draftFilter = new LightingColorFilter(Color.BLACK, Color.GRAY);
        drawable_draft.setColorFilter(drawable_draftFilter);
        //jika status disimpan, maka icon disket
        if(cardModelList.get(position).getStatus() == Activity.RESULT_CANCELED){
            tv_lastEdit.setCompoundDrawablesWithIntrinsicBounds(null,null,drawable_draft,null);
            //jika status sukses, maka icon checklist
        } else {
            tv_lastEdit.setCompoundDrawablesWithIntrinsicBounds(null,null,drawable_success,null);
        }

        tv_title.setText(cardModelList.get(position).getTitle());
        String latestEditTime = Utils.getTimestampHourMinutesStringByTimeMillis(cardModelList.get(position).getLastEdit());
        tv_lastEdit.setText(latestEditTime);

        holder.selectedOverlay.setVisibility(isSelected(position) ? View.VISIBLE : View.INVISIBLE);
        tv_preview.setText(cardModelList.get(position).getPreview());
    }

    /**
     * Menghapus list dengan id tertentu
     * @param position
     */
    public void deleteCard(int position){
        try{
            listFormView.removeData(cardModelList.get(position).getLocalId());
            cardModelList.remove(position);
            notifyItemRemoved(position);
            listFormView.checkEmptyList();
        }catch (IndexOutOfBoundsException e){
            Toast.makeText(context, "Kesalahan dalam Index Menghapus H01", Toast.LENGTH_SHORT).show();
        }
    }

    //mengurutkan card berdasarkan waktu
    public void sort(){
        Collections.sort(cardModelList);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return cardModelList.size();
    }

    public int searchIdxCardFromLocalId(int id){
        for(int i = 0; i<cardModelList.size();i++){
            if(id == cardModelList.get(i).getLocalId())
                return i;
        }
        return -1;
    }

    public ListFormCardModel getCardModel(int idx){
        return this.cardModelList.get(idx);
    }

    public void updateCardModel(int idx, ListFormCardModel cardModel){
        this.cardModelList.set(idx, cardModel);
    }

    public void add(ListFormCardModel cardModel){
        this.cardModelList.add(cardModel);
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener,
            View.OnLongClickListener {
        @SuppressWarnings("unused")
        private static final String TAG = CardViewHolder.class.getSimpleName();

        TextView tv_title;
        TextView tv_lastEdit;
        View selectedOverlay;
        public ClickListener listener;
        TextView tv_preview;

        public CardViewHolder(View itemView, ClickListener listener){
            super(itemView);
            tv_title            = (TextView) itemView.findViewById(R.id.tv_cv_title);
            tv_lastEdit         = (TextView) itemView.findViewById(R.id.tv_cv_last_edit);
            selectedOverlay     = itemView.findViewById(R.id.selected_overlay);
            selectedOverlay.getBackground().setAlpha(128);
            this.listener       = listener;
            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
            tv_preview = (TextView) itemView.findViewById(R.id.tv_cv_preview);
        }

        @Override
        public void onClick(View v) {
            if(listener != null) {
                listener.onItemClicked(getPosition());
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if(listener != null){
                listener.onItemLongClicked(getPosition());
            }
            return false;
        }

        public interface ClickListener{
            public void onItemClicked(int position);
            public boolean onItemLongClicked(int position);
        }
    }
}
