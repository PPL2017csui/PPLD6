package com.ppld6.marumaru.pelaporanruma.login;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ppld6.marumaru.pelaporanruma.MyApplication;
import com.ppld6.marumaru.pelaporanruma.R;
import com.ppld6.marumaru.pelaporanruma.listForm.ListFormActivity;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginView {
    LoginPresenter loginPresenter;
    ProgressDialog pd_login;
    @BindView(R.id.et_phone_number)EditText et_phoneNumber;
    @BindView(R.id.et_password) EditText et_password;
    @BindView(R.id.btn_login) Button btn_login;
    Snackbar snackBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FirebaseApp.initializeApp(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initProgressDialog();
        loginPresenter = new LoginPresenterImpl(this);
        initSnackbar();
    }

    public void checkConnection(){
        MyApplication.getInstance().connectionListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean firebaseConnected = snapshot.getValue(Boolean.class);
                if (firebaseConnected) {
                    btn_login.setEnabled(true);
                    btn_login.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    snackBar.dismiss();
                } else {
                    snackBar.show();
                    btn_login.setEnabled(false);
                    btn_login.setBackgroundColor(Color.GRAY);
                    if(pd_login.isShowing()){
                        pd_login.dismiss();
                        Toast.makeText(getApplicationContext(), "Koneksi Terputus", Toast.LENGTH_SHORT).show();
                    }
                }
                Log.d("connection", firebaseConnected+"");
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });

    }

    public void initProgressDialog(){
        pd_login = new ProgressDialog(this);
        pd_login.setTitle("Logged In");
        pd_login.setMessage("Please Wait...");
        pd_login.setCancelable(false);
    }

    @OnClick(R.id.btn_login)
    public void attempLogin(){
        String phoneNumber = et_phoneNumber.getText().toString();
        String password = et_password.getText().toString();
        loginPresenter.authenticate(phoneNumber, password);
    }


    @Override
    public void showProgress() {
        pd_login.show();
    }

    @Override
    public void hideProgress() {
        pd_login.dismiss();
    }

    @Override
    public void showError(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToHome() {
        Intent intent = new Intent(this, ListFormActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkConnection();
        loginCheck();
    }

    public void loginCheck(){
        if(Utils.isLoggedIn(this)){
            Intent intent = new Intent(this, ListFormActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public void clearPasswordField(){
        et_password.setText("");
    }

    public void initSnackbar(){
        snackBar = Snackbar.make(findViewById(R.id.layout_coordinator), "Koneksi Terputus", Snackbar.LENGTH_INDEFINITE);
        snackBar.setActionTextColor(Color.WHITE);
    }
    public void resetActivity(){
        startActivity(new Intent(this, LoginActivity.class));
    }
}
