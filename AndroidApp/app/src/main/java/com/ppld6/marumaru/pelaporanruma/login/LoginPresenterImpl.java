package com.ppld6.marumaru.pelaporanruma.login;

/**
 * Created by jefly on 25/03/17.
 */

public class LoginPresenterImpl implements LoginPresenter {
    private LoginView loginView;
    private LoginInteractor loginInteractor;
    private final String EMAIL_DOMAIN = "@marumaru.com";

    LoginPresenterImpl(LoginView loginView){
        this.loginView = loginView;
        this.loginInteractor = new LoginInteractorImpl();
    }

    @Override
    public void authenticate(String phoneNumber, String password) {
        loginView.showProgress();
        loginInteractor.login(phoneNumber+EMAIL_DOMAIN, password, new OnLoginFinishedListener());
    }

    class OnLoginFinishedListener implements LoginInteractor.OnLoginFinishedListener{

        @Override
        public void onError(String message) {
            loginView.hideProgress();
            loginView.showError(message);
            loginView.clearPasswordField();
        }

        @Override
        public void onSuccess() {
            loginView.hideProgress();
            loginView.navigateToHome();
        }
    }
}
