package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.opengl.GLSurfaceView;
import android.os.Build;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by kelvin on 05/04/17.
 */

public class ShortTextField extends FormField {

    private int maxLength;

    public ShortTextField(int id, String label, String description, String placeholder, boolean required, int maxLength, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
        this.maxLength = maxLength;
    }

    public ShortTextField(int id, String name, String label, String description, String placeholder, boolean required, int maxLength, int order, boolean enabled) {
        super(id,name, label, description, placeholder, required, order, enabled);
        this.maxLength = maxLength;
    }

    @Override
    public boolean validate() {
        int errType = 1;
        if(required) {
            if ( ((String) result()).isEmpty()) {
                errorCode = " (" + label.charAt(0) + "" + id + 0 + errType + ")";
                return false;
            }
            errType++;
            if (((String) result()).length() > maxLength) {
                errorCode = " (" + label.charAt(0) + "" + id + 0 + errType + ")";
                return false;
            }
        }
        return true;
    }

    @Override
    public View render(Context context) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0,20,0,0);
        layout.setLayoutParams(layoutParams);

        EditText et_stf = new EditText(context);
        if(!enabled){
            et_stf.setFocusable(false);
            et_stf.setEnabled(false);
        }
        et_stf.setHint(label);
        et_stf.setHintTextColor(Color.GRAY);
        view = et_stf;
        TextInputLayout til_et_stf = new TextInputLayout(context);
        til_et_stf.addView(view);
        view = til_et_stf;
        layout.addView(view);
        TextView tv_label = new TextView(context);
        tv_label.setTextColor(Color.GRAY);
        tv_label.setPadding(10,0,0,0);
        tv_label.setTypeface(tv_label.getTypeface(), Typeface.BOLD);
        if(description!= null && !description.isEmpty()) {
            tv_label.setText(description);
        }
        if(required)
            tv_label.setText(tv_label.getText().toString()+" (required)");
        layout.addView(tv_label);
        layout.setPadding(10,0,30,10);
        return layout;
    }

    @Override
    public Object result() {
        return ((TextInputLayout)view).getEditText().getText().toString();
    }

    public void setView(View view) {
        super.view = view;
    };



}
