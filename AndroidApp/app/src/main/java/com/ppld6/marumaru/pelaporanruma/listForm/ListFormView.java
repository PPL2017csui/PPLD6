package com.ppld6.marumaru.pelaporanruma.listForm;

import android.content.DialogInterface;

/**
 * Created by jefly on 26/04/17.
 */

public interface ListFormView {
    public void checkEmptyList();
    public void removeData(int id);
}
