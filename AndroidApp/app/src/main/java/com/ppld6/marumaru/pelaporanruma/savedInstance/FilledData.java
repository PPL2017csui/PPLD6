package com.ppld6.marumaru.pelaporanruma.savedInstance;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jefly on 13/05/17.
 */

public class FilledData {
    public String title;
    public long lastEdit;
    public int id;
    public int status;
    public String previewText;
    public Map<Integer, String> answerMap;

    public FilledData(int id, String title){
        this.id = id;
        this.title = title;
        answerMap = new HashMap<>();
    }
}
