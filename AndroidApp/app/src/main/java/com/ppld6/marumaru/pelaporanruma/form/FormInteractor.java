package com.ppld6.marumaru.pelaporanruma.form;

import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.UploadTask;
import com.ppld6.marumaru.pelaporanruma.form.model.FormModel;
import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;

/**
 * Created by jefly on 25/03/17.
 */

public interface FormInteractor {
    public FormModel getFormModel();
    public String submitFormFields(DatabaseReference.CompletionListener completionListener);
    public void addModel(FormField f);
    public void getFormModelFromFirebase(ValueEventListener listener);
    public void clearAllUploadTask();
}
