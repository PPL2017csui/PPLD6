package com.ppld6.marumaru.pelaporanruma.form;

import android.content.DialogInterface;

import com.ppld6.marumaru.pelaporanruma.form.model.FormModel;

/**
 * Created by jefly on 27/03/17.
 */

public interface FormView {
    public void renderForm(FormModel formModel);
    public void showProgress();
    public void hideProgress();
    public void showToast(String messages);
    public void setSuccessResult();
    public void showLoadingFormProgressBar();
    public void hideLoadingFormProgressBar();
}
