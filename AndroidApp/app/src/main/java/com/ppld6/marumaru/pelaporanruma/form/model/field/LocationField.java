package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.ppld6.marumaru.pelaporanruma.R;
import com.squareup.picasso.Picasso;

import static android.app.Activity.RESULT_OK;

/**
 * Created by jefly on 05/04/17.
 */

public class LocationField extends SpecialCaseWhenOfflineField {
    public Drawable drawable;
    public EditText et_map;
    public LocationField(int id, String label, String description, String placeholder, boolean required, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
    }

    public LocationField(int id, String name, String label, String description, String placeholder, boolean required, int order, boolean enabled) {
        super(id, name, label, description, placeholder, required, order,enabled);
    }

    @Override
    public boolean validate() {
        int errType = 1;
        if(required && ((String)result()).isEmpty()) {
            errorCode = " ("+label.charAt(0) + "" + id + 0 + errType+")";
            return false;
        }
        else return true;
    }

    @Override
    public View render(final Context context) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 20, 0, 0);
        layout.setLayoutParams(layoutParams);


        et_map = new EditText(context);
        et_map.setHint(label);
        et_map.setId(id);
        et_map.setMinLines(2);
        drawable = context.getResources().getDrawable(R.drawable.ic_location_on_black_24dp);
        ColorFilter filter = new LightingColorFilter(Color.BLACK, context.getResources().getColor(R.color.colorPrimary));
        drawable.setColorFilter(filter);

        et_map.setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
        if (enabled){
            et_map.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    final int DRAWABLE_RIGHT = 2;
                    if (event.getAction() == MotionEvent.ACTION_UP) {
                        if (event.getRawX() >= (et_map.getRight() - et_map.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                            startPlacePicker(id, context);
                            return true;
                        }
                    }
                    return false;
                }
            });
        } else {
            et_map.setFocusable(false);
            et_map.setEnabled(false);
        }
        view = et_map;
        TextInputLayout til_et_lf = new TextInputLayout(context);
        til_et_lf.addView(view);
        view = til_et_lf;
        layout.addView(view);

        TextView tv_label = new TextView(context);
        tv_label.setTextColor(Color.GRAY);
        tv_label.setPadding(10,0,0,0);
        tv_label.setTypeface(tv_label.getTypeface(), Typeface.BOLD);
        if(description!= null && !description.isEmpty()) {
            tv_label.setText(description);
        }
        if(required)
            tv_label.setText(tv_label.getText().toString()+" (required)");
        layout.addView(tv_label);
        layout.setPadding(10,0,30,10);
        return layout;
    }

    public void startPlacePicker(int requestCode, Context context){
        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        try {
            ((Activity)context).startActivityForResult(builder.build((Activity)context), requestCode);
        } catch (GooglePlayServicesRepairableException e) {
            e.printStackTrace();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Object result() {
        return ((TextInputLayout) view).getEditText().getText().toString();
    }

    @Override
    public void activityResult(int resultCode, Intent data, Context context) {
        if(resultCode == RESULT_OK) {
            Place p = PlacePicker.getPlace(data, context);
            if (p != null) {
                TextInputLayout til =(TextInputLayout) view;
                til.getEditText().setText(p.getAddress());
            }
        }
    }

    public void setView(View view){this.view = view;}

    public Drawable getDrawable(){
        return this.drawable;
    }

    public void enableIcon(final Context context){
        et_map.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_RIGHT = 2;

                if (event.getAction() == MotionEvent.ACTION_UP) {
                    if (event.getRawX() >= (et_map.getRight() - et_map.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        startPlacePicker(id, context);
                        return true;
                    }
                }
                return false;
            }
        });

    }
    public void disableIcon(final Context context){
        et_map.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return false;
            }
        });

    }
}
