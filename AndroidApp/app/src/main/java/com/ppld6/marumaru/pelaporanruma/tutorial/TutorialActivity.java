package com.ppld6.marumaru.pelaporanruma.tutorial;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.ppld6.marumaru.pelaporanruma.R;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TutorialActivity extends AppCompatActivity {
    @BindView(R.id.wv_tutorial)
    WebView wb;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        wb.getSettings().setJavaScriptEnabled(true);

        wb.getSettings().setAppCacheEnabled(true);
        wb.setWebChromeClient(new WebChromeClient());
        wb.loadUrl("http://panduan-marumaru.000webhostapp.com/panduan_aplikasi_ruma/panduan_aplikasi_ruma/index.html");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Utils.checkActiveUser(this);
    }
}
