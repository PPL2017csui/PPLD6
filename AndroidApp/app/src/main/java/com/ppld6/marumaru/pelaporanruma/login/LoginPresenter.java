package com.ppld6.marumaru.pelaporanruma.login;

/**
 * Created by jefly on 25/03/17.
 */

public interface LoginPresenter {
    void authenticate(String phoneNumber, String password);
}
