package com.ppld6.marumaru.pelaporanruma.form;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.FirebaseApp;
import com.ppld6.marumaru.pelaporanruma.MyApplication;
import com.ppld6.marumaru.pelaporanruma.R;
import com.ppld6.marumaru.pelaporanruma.form.model.FormModel;
import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LocationField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.SpecialCaseWhenOfflineField;
import com.ppld6.marumaru.pelaporanruma.login.LoginActivity;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activity untuk menampilkan pengisian Form
 */
public class FormActivity extends AppCompatActivity implements
         FormView{

    @BindView(R.id.layout_form_wrapper)
    LinearLayout formLayout;

    /*layout untuk snackbar*/
    @BindView(R.id.layout_coordinator)
    CoordinatorLayout layout_coordinator;

    /*layout ditampilkan ketika loading form dari database*/
    @BindView(R.id.layout_loading)
    RelativeLayout loadingLayout;


    FormPresenter formPresenter;

    /*List dari field*/
    ArrayList<FormField> formFields;
    ProgressDialog pd_submit;

    Snackbar snackBar;
    Button btn_submit;
    boolean success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fill_form);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);
        initSubmitProgressDialog();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Intent intent = this.getIntent();
        if(intent.getBooleanExtra("success", false)){
            this.success = true;
        }

        int rv_id = getIntent().getIntExtra("id_rv", -1);
        formPresenter = new FormPresenterImpl(rv_id, this, this);
        initSnackbar();
        if(success) {
            formPresenter.generateForm(false);
        } else {
            formPresenter.generateForm(true);
        }
    }

    public void initSnackbar(){
        snackBar = Snackbar.make(findViewById(R.id.layout_coordinator), "Koneksi Terputus", Snackbar.LENGTH_INDEFINITE);
        snackBar.setAction("Tutup", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackBar.dismiss();
            }
        });
        snackBar.setActionTextColor(Color.WHITE);
    }

    /*Mengecek koneksi, jika tidak tersambung disable button dan tampilkan snackbar*/
    public void checkConnection(){
        MyApplication.getInstance().connectionListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                boolean connected = snapshot.getValue(Boolean.class);
                if (connected) {
                    snackBar.dismiss();
                    showLocationIcon();
                    if(btn_submit!=null) {
                        btn_submit.setEnabled(true);
                        btn_submit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                    }
                } else {
                    if(btn_submit!= null){
                        btn_submit.setBackgroundColor(Color.GRAY);
                        btn_submit.setEnabled(false);
                    }
                    snackBar.show();
                    hideLocationIcon();
                    if (pd_submit.isShowing()) {
                        pd_submit.dismiss();
                        showToast("Gagal Koneksi Terputus");
                        formPresenter.clearAllUploadTask();
                    }
                }
                Log.d("connection", connected+"");
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.err.println("Listener was cancelled");
            }
        });
    }

    public void initSubmitProgressDialog(){
        pd_submit = new ProgressDialog(this);
        pd_submit.setTitle("Submitting");
        pd_submit.setMessage("Please Wait...");
        pd_submit.setCancelable(false);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        /*Jika form berhasil di load, dan user pencet tombol back, maka form tersimpan*/
        if(formPresenter.isLoaded() && !success) {
            formPresenter.saveData(Activity.RESULT_CANCELED);
            setResult(Activity.RESULT_CANCELED, null);
            finish();
        /*Jika tidak, tidak perlu disimpan*/
        } else {
            setResult(Activity.DEFAULT_KEYS_SHORTCUT, null);
            finish();
        }
    }

    public void showLoadingFormProgressBar(){
        formLayout.setVisibility(View.GONE);
        loadingLayout.setVisibility(View.VISIBLE);
    }

    public void hideLoadingFormProgressBar(){
        formLayout.setVisibility(View.VISIBLE);
        loadingLayout.setVisibility(View.GONE);
    }

    @Override
    public void renderForm(FormModel formModel) {
        formLayout.removeAllViews();
        formFields = formModel.listAllFields();
        Collections.sort(formFields);
        for(FormField fieldModel : formFields){
            View view = fieldModel.render(this);
            formLayout.addView(view);
        }

        LinearLayout btnLayout = new LinearLayout(this);
        btnLayout.setPadding(0,20,0,0);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);

        layoutParams.setMargins(0,20,0,0);
        btnLayout.setLayoutParams(layoutParams);
        btn_submit = new Button(this);
        btn_submit.setText("Kirim");
        btn_submit.setTextColor(Color.WHITE);
        btn_submit.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        btn_submit.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 20);
        btn_submit.setPadding(0,10,0,10);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                formPresenter.submitForm();
            }
        });
        btn_submit.setLayoutParams(layoutParams);
        btnLayout.addView(btn_submit);
        if(!success)
            formLayout.addView(btnLayout);
    }

    @Override
    public void showProgress() {
        pd_submit.show();
    }

    @Override
    public void hideProgress() {
        pd_submit.dismiss();
    }

    @Override
    public void showToast(String messages) {
        Toast.makeText(this, messages, Toast.LENGTH_SHORT).show();
    }


    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //set activity result untuk map, dan lokasi
        for(FormField field : formFields){
            if(field.getId() == requestCode){
                ((SpecialCaseWhenOfflineField) field).activityResult(resultCode,data,this);
            }
        }
    }

    public void loginCheck() {
        if (!Utils.isLoggedIn(this)) {
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkConnection();
        loginCheck();
        Utils.checkActiveUser(this);
    }

    /**
     * Jika berhasil submit, maka method ini akan menyimpan hasil dan kembali ke halaman sebelumnya
     * dengan flag RESULT_OK
     */
    @Override
    public void setSuccessResult() {
        formPresenter.saveData(Activity.RESULT_OK);
        setResult(Activity.RESULT_OK, null);
        finish();
    }


    public void hideLocationIcon(){
        if(formFields!=null) {
            for (FormField field : formFields) {
                if (field instanceof LocationField) {
                    ((TextInputLayout) ((LocationField) field).getView()).getEditText().setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);
                    ((LocationField) field).disableIcon(this);
                }
            }
        }
    }

    public void showLocationIcon(){
        if(formFields!=null) {
            Drawable drawable = this.getResources().getDrawable(R.drawable.ic_location_on_black_24dp);
            ColorFilter filter = new LightingColorFilter(Color.BLACK, this.getResources().getColor(R.color.colorPrimary));
            drawable.setColorFilter(filter);
            for (FormField field : formFields) {
                if (field instanceof LocationField) {
                    ((TextInputLayout) ((LocationField) field).getView()).getEditText().setCompoundDrawablesWithIntrinsicBounds(null, null, drawable, null);
                    ((LocationField) field).enableIcon(this);
                }
            }
        }
    }
}
