package com.ppld6.marumaru.pelaporanruma.listForm.model;

import android.graphics.drawable.Drawable;

import com.ppld6.marumaru.pelaporanruma.R;
import android.support.annotation.NonNull;

/**
 * Created by jefly on 23/04/17.
 */

public class ListFormCardModel implements Comparable<ListFormCardModel>{
    private int status;
    private long lastEdit;
    private String title;
    private int localId;
    private String preview;
    public ListFormCardModel(int localId, int status, long lastEdit, String title, String preview) {
        this.localId = localId;
        this.status = status;
        this.lastEdit = lastEdit;
        this.title = title;
        this.preview = preview;
    }
    public long getLastEdit() {
        return lastEdit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStringStatus(){
        switch(status){
            case 0 :
                return "Disimpan";
            case -1 :
                return "Sukses";
        }
        return "";
    }

    public String getPreview(){
        return this.preview;
    }

    public int getLocalId() {
        return localId;
    }

    public int getStatus(){return status;}
    @Override
    public int compareTo(@NonNull ListFormCardModel o) {
        return (int)(o.lastEdit - this.lastEdit);
    }
}
