package com.ppld6.marumaru.pelaporanruma.login;

/**
 * Created by jefly on 25/03/17.
 */

public interface LoginInteractor {
    interface OnLoginFinishedListener {
        void onError(String message);
        void onSuccess();
    }

    void login(String phoneNumber, String password, OnLoginFinishedListener listener);
}
