package com.ppld6.marumaru.pelaporanruma.form;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.ppld6.marumaru.pelaporanruma.form.model.FormModel;
import com.ppld6.marumaru.pelaporanruma.form.model.field.FormField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LocationField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.LongTextField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.NumberField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.PhoneNumberField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.PictureField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.ShortTextField;
import com.ppld6.marumaru.pelaporanruma.form.model.field.SpecialCaseWhenOfflineField;
import com.ppld6.marumaru.pelaporanruma.login.LoginActivity;
import com.ppld6.marumaru.pelaporanruma.splash.SplashActivity;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by jefly on 26/03/17.
 */

public class FormInteractorImpl implements FormInteractor {
    Context context;
    FormModel formModel;
    FirebaseStorage storage;
    StorageReference storageRef;
    FirebaseDatabase database;
    FirebaseAuth auth;
    DatabaseReference databaseRef;
    public FormInteractorImpl(Context context){
        this.context = context;
        storage = FirebaseStorage.getInstance();
        database = FirebaseDatabase.getInstance();
        auth = FirebaseAuth.getInstance();
        storageRef = storage.getReferenceFromUrl("gs://marumaru-b2f9a.appspot.com");
        databaseRef = database.getReference();
        formModel = new FormModel();
    }

    @Override
    public FormModel getFormModel() {
        return formModel;
    }

    /**
     * Mengirim gambar
     * @param path
     * @param onsuccess listener ketika sukses
     * @param onfailure listener ketika gagal
     */
    public void uploadPicture(String path, OnSuccessListener<UploadTask.TaskSnapshot> onsuccess, OnFailureListener onfailure){
        Log.d("submit", path);
        Uri file = Uri.fromFile(new File(path));
        StorageReference bmmRef = storageRef.child("images/bmm/"+Utils.getUser().getUid()+"_"+System.currentTimeMillis()+".jpg");
        Log.d("submit", "images/bmm/"+Utils.getUser().getUid()+"_"+ System.currentTimeMillis()+".jpg");
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType("image/jpg")
                .build();
        UploadTask uploadTask = bmmRef.putFile(file, metadata);
        uploadTask.addOnFailureListener(onfailure).addOnSuccessListener(onsuccess);
    }

    /*jumlah gambar yang telah terkirim*/
    private int numPicture = 0;
    /*error yang ada dari suatu field*/
    String errorField = null;

    /**
     * submit hasil input ke database, keluaran dari method ini berupa String
     * jika null maka tidak terdapat error
     * @param completionListener
     * @return
     */
    @Override
    public String submitFormFields(final DatabaseReference.CompletionListener completionListener) {
        ArrayList<FormField> formFields = getFormModel().listAllFields();

        //temp2 untuk menyimpan filled_form yang berisi waktu pengiriman, user id, dan hasil value dari field
        final Map<String, Object> temp2 = new HashMap<>();
        //temp berisi nama field dan valuenya
        final Map<String, String> temp = new HashMap<>();

        errorField = null;
        numPicture = 0;

        //menghitung jumlah gambar yang akan dikirim
        for(FormField field : formFields){
            if(field instanceof PictureField){
                numPicture++;
            }
        }
        Log.d("submit", numPicture+"");
        for(final FormField field : formFields) {
            //validasi dari field
            if(!field.validate()){
                errorField = field.getLabel()+ field.getErrorCode();
                return errorField;
            } else {
                //jika value berupa string, maka langsung disimpan dalam map
                if (field instanceof LocationField || field instanceof ShortTextField || field instanceof LongTextField || field instanceof PhoneNumberField) {
                    temp.put(field.getName(), (String) field.result());
                // jika gambar, maka lakukan uploading
                } else if(field instanceof PictureField){
                    Log.d("submit" , "ok"+((PictureField) field).getPath());
                    uploadPicture(((PictureField) field).getPath(), new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            numPicture--;
                            @SuppressWarnings("VisibleForTests") Uri download = taskSnapshot.getDownloadUrl();
                            temp.put(field.getName(), download.toString());
                        }
                    }, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            numPicture = -100;
                        }
                    });
                }
            }
        }
        final Handler h = new Handler();
        final int INTERVAL = 100;
        //lakukan iterasi dengan delay 100ms per iterasi sampai gambar berhasil disimpan
        h.postDelayed(new Runnable() {
            @Override
            public void run() {
                //jika tidak error
                if (errorField == null) {
                    //jika gambar sudah diupload seluruhnya
                    if(numPicture == 0) {
                        //push satu child ke filled_form
                        final String key = databaseRef.child("filled_form").push().getKey();
                        //map untuk diinput ke database
                        Map<String, Object> updateVal = new HashMap<>();
                        temp2.put("waktu", System.currentTimeMillis()+"");
                        temp2.put("user", auth.getCurrentUser().getUid());
                        temp2.put("fields", temp);
                        temp2.put("form_version", getFormModel().getVersion());
                        temp2.put("form_id", getFormModel().getId());
                        updateVal.put("/filled_form/" + key, temp2);
                        databaseRef.updateChildren(updateVal, completionListener);
                    //jika -100 (artinya gagal merubah gambar)
                    } else if(numPicture == -100){
                        errorField = "Gagal mengunggah gambar.";
                    } else {
                        h.postDelayed(this, INTERVAL);
                    }
                }
            }
        }, INTERVAL);
        return errorField;
    }

    /**
     * Menambah field baru f
     * @param f
     */
    @Override
    public void addModel(FormField f) {
        this.formModel.addField(f);
    }

    /**
     * Mengambil form dari database dengan listener tertentu
     * @param listener
     */
    public void getFormModelFromFirebase(ValueEventListener listener){
        /*mengubah reference ke form_templates*/
        DatabaseReference ref = databaseRef.child("form_templates");
        ref.keepSynced(true);
        ref.addValueEventListener(listener);
    }

    /**
     * Membersihkan task upload (ketika error, keseluruhan task akan dibersihkan)
     */
    public void clearAllUploadTask(){
        List<UploadTask> uploadTasks = storageRef.getActiveUploadTasks();
        for(UploadTask task : uploadTasks){
            task.cancel();
        }
    }
}
