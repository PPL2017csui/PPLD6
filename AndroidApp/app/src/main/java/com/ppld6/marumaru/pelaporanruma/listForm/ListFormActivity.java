package com.ppld6.marumaru.pelaporanruma.listForm;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ppld6.marumaru.pelaporanruma.R;
import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.listForm.model.ListFormCardModel;
import com.ppld6.marumaru.pelaporanruma.login.LoginActivity;
import com.ppld6.marumaru.pelaporanruma.savedInstance.FilledData;
import com.ppld6.marumaru.pelaporanruma.tutorial.TutorialActivity;
import com.ppld6.marumaru.pelaporanruma.utils.SharedPreferencesManager;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ListFormActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ListFormView, ListFormAdapter.CardViewHolder.ClickListener {
    @BindView(R.id.rv_list_form) RecyclerView rv_listForm;

    private ListFormAdapter rv_adapter;
    private ActionMode actionMode;
    private ActionModeCallBack actionModeCallBack = new ActionModeCallBack();
    private RecyclerView.LayoutManager rv_layoutManager;
    private ArrayList<ListFormCardModel> cardModelList;
    //merupakan local id saat ini
    private int numForm;
    @BindView(R.id.iv_empty_text) ImageView iv_empty_text;
    private NavigationView navigationView;
    @BindView(R.id.layout_empty_text)
    RelativeLayout layout_emptyText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_form);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //mengambil local id
        numForm = SharedPreferencesManager.currentFormId(this);
        initEmptyLayout();
        initRecyclerView();
        showUserData();
    }

    /**
     * inisialisasi tampilan jika tidak ada field
     */
    public void initEmptyLayout(){
        Picasso.with(this).load(R.drawable.sun).resize(Utils.getScreenWidth(this)/3, Utils.getScreenWidth(this)/3).centerInside().into(iv_empty_text);
    }

    /**
     * menampilkan user data (email, no telepon)
     */
    public void showUserData(){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        View headerView = navigationView.getHeaderView(0);
        TextView tv_username = (TextView)headerView.findViewById(R.id.tv_username);
        tv_username.setText(auth.getCurrentUser().getDisplayName());
        TextView tv_noTelepon = (TextView)headerView.findViewById(R.id.tv_no_telepon);
        tv_noTelepon.setText(auth.getCurrentUser().getEmail().replace("@marumaru.com",""));
    }

    /**
     * inisialisasi recyclerview
     */
    public void initRecyclerView(){
        rv_layoutManager =  new LinearLayoutManager(this);;
        initCardModel();
        rv_adapter = new ListFormAdapter(cardModelList, this, this, this);
        rv_listForm.setHasFixedSize(true);
        rv_listForm.setAdapter(rv_adapter);
        rv_listForm.setItemAnimator(new DefaultItemAnimator());
        rv_listForm.setLayoutManager(rv_layoutManager);
    }

    public void initCardModel(){
        cardModelList = new ArrayList<>();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.list_form, menu);
        return true;
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_logout) {
            logout();
        } else if(id == R.id.nav_tutorial){
            goToTutorial();
        } else if(id == R.id.nav_call){
            callCS();
        } else if(id == R.id.nav_about){
            showAboutDialog();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /**
     * menelepon customer service
     */
    public void callCS(){
        startActivity(new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+getResources().getString(R.string.cs_phone_number))));
    }

    /**
     * Menuju halaman tutorial
     */
    public void goToTutorial(){
        Intent i = new Intent(this, TutorialActivity.class);
        startActivity(i);
    }

    /**
     * Menambah laporan baru
     */
    @OnClick(R.id.btn_isi)
    public void isi(){
        //batas id maksimum adalah 1000
        if(numForm>1000){
            Toast.makeText(this, "Tidak bisa melebihi 1000 form",Toast.LENGTH_LONG).show();
            return;
        }

        Intent i = new Intent(this, FormActivity.class);
        //mengirimkan id local ke activity pengisian form
        i.putExtra("id_rv", numForm);
        startActivityForResult(i, numForm++);
        //increment form id di sharedpreferences
        SharedPreferencesManager.formIdAutoIncrement(this);
    }


    public void checkEmptyList(){
        if(rv_adapter.getItemCount() == 0){
            layout_emptyText.setVisibility(View.VISIBLE);
        } else {
            layout_emptyText.setVisibility(View.GONE);
        }
    }

    public void logout(){
        FirebaseAuth.getInstance().signOut();
        Intent i = new Intent(this, LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //check form expired atau tidak
        checkFormExpiredDate();
        //load list form yang sedang diisi
        loadSavedData();
        //check bahwa list kosong
        checkEmptyList();
        Utils.checkActiveUser(this);
    }

    /**
     * Jika berbeda hari maka form akan dihapus
     */
    public void checkFormExpiredDate(){
        if(!SharedPreferencesManager.currentDate(this).equals(Utils.getDateStringByTimeMillis(System.currentTimeMillis()))){
            SharedPreferencesManager.clear(this);
            SharedPreferencesManager.updateCurrentDate(this);
        }
    }

    /**
     * Menampilkan dialog about
     */
    public void showAboutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.dialog_about, null);
        ImageView iv_logo = (ImageView) dialogView.findViewById(R.id.iv_dialog_logo);
        Picasso.with(this).load(R.drawable.ic_launcher).centerInside().resize(Utils.getScreenWidth(this) / 3, Utils.getScreenWidth(this) / 3).into(iv_logo);
        builder.setView(dialogView);
        builder.show();
    }

    /**
     * Load list dari form yang telah diisi
     */
    public void loadSavedData(){
        Gson gson = new Gson();
        Type type = new TypeToken<List<FilledData>>(){}.getType();
        String json = SharedPreferencesManager.getFormAnswer(this);
        //jika masih kosong
        if(!json.isEmpty()){
            List<FilledData> listFilledData = gson.fromJson(json,type);
            //iterasi list form yang telah diisi
            for(FilledData filledData : listFilledData){
                boolean flag = false;
                //update element dengan id yang telah diisi
                for(int i = 0; i<rv_adapter.getItemCount(); i++){
                    if(rv_adapter.getCardModel(i).getLocalId() == filledData.id){
                        rv_adapter.updateCardModel(i, new ListFormCardModel(filledData.id,filledData.status, filledData.lastEdit, filledData.title, filledData.previewText));
                        flag = true;
                    }
                }
                if(!flag){
                    rv_adapter.add(new ListFormCardModel(filledData.id,filledData.status, filledData.lastEdit, filledData.title, filledData.previewText));
                    rv_listForm.scrollToPosition(0);
                }
            }
        }
        rv_adapter.sort();
    }

    /**
     * Membuang data dengan id tertentu
     * @param id
     */
    public void removeData(int id){
        Gson gson = new Gson();
        Type type = new TypeToken<List<FilledData>>(){}.getType();
        String json = SharedPreferencesManager.getFormAnswer(this);
        List<FilledData> listFilledData = gson.fromJson(json,type);
        for(int i = 0; i<listFilledData.size(); i++){
            if(listFilledData.get(i).id == id){
                listFilledData.remove(i);
                break;
            }
        }
        json = gson.toJson(listFilledData, type);
        Log.d("delete", json);
        SharedPreferencesManager.storeFormAnswer(json, this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == Activity.RESULT_CANCELED){
            showToast("Draft Tersimpan");
        }
    }

    public void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public ListFormAdapter getRv_adapter() {
        return rv_adapter;
    }

    @Override
    public void onItemClicked(int position) {
        if(actionMode == null){
            ListFormCardModel cardModel = rv_adapter.getCardModel(position);
            int id = cardModel.getLocalId();
            Intent i = new Intent(getApplicationContext(), FormActivity.class);
            //kirim id ke form activity
            i.putExtra("id_rv", cardModel.getLocalId());
            if(cardModelList.get(position).getStatus() == Activity.RESULT_CANCELED) {
                i.putExtra("success", false);
            } else {
                i.putExtra("success", true);
            }
            startActivityForResult(i, cardModel.getLocalId());

        }else{
            toggleSelection(position);
        }
    }

    @Override
    public boolean onItemLongClicked(int position) {
        if (actionMode == null){
            actionMode = startSupportActionMode(actionModeCallBack);
        }
        toggleSelection(position);
        return true;
    }

    public void toggleSelection(int position){
        rv_adapter.toggleSelection(position);
        int counter = rv_adapter.getSelectedItemCount();

        if (counter == 0){
            actionMode.finish();
        }else {
            actionMode.setTitle(String.valueOf(counter));
            actionMode.invalidate();
        }
    }

    private class ActionModeCallBack implements ActionMode.Callback {
        @SuppressWarnings("unused")
        private final String TAG = ActionModeCallBack.class.getSimpleName();

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            menu.clear();
            mode.getMenuInflater().inflate(R.menu.selected_menu , menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()){
                case R.id.menu_remove:
                    int totalItem = rv_adapter.getSelectedItemCount();
                    List<Integer> selectedItems = rv_adapter.getSelectedItems();
                    for(int i = totalItem-1 ; i >= 0; i--){
                        rv_adapter.deleteCard(selectedItems.get(i));
                    }
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            rv_adapter.clearSelection();
            actionMode = null;
        }
    }
}


