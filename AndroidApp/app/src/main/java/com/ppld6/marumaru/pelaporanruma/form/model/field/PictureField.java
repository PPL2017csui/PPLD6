package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.esafirm.imagepicker.features.ImagePicker;
import com.esafirm.imagepicker.features.ImagePickerActivity;
import com.esafirm.imagepicker.model.Image;
import com.ppld6.marumaru.pelaporanruma.R;
import com.ppld6.marumaru.pelaporanruma.form.FormActivity;
import com.ppld6.marumaru.pelaporanruma.utils.Utils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

/**
 * Created by james on 05/04/17.
 */

public class PictureField extends SpecialCaseWhenOfflineField {
    protected boolean filled;
    protected String path="";

    public PictureField(int id, String label, String description, String placeholder, boolean required, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
        if(!required)
            filled = true;
        else
            filled = false;
    }

    public PictureField(int id, String name, String label, String description, String placeholder, boolean required, int order, boolean enabled) {
        super(id,name, label, description, placeholder, required, order, enabled);
        if(!required)
            filled = true;
        else
            filled = false;
    }

    @Override
    public boolean validate() {
        int errType=1;
        if(!filled) {
            errorCode = " ("+label.charAt(0) + "" + id + 0 + errType+")";
            return false;
        }
        return true;
    }

    @Override
    public View render(final Context context) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0,0,0);
        layout.setLayoutParams(layoutParams);


        ImageView iv_picture = new ImageView(context);
        iv_picture.setImageResource(R.drawable.add);
        Picasso.with(context).load(R.drawable.add).resize(Utils.getScreenWidth(context)/3, Utils.getScreenWidth(context)/3).into(iv_picture);
        iv_picture.setPadding(20,0,20,0);
        if(enabled) {
            iv_picture.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startImageCapture(id, context);
                }
            });
        }
        view = iv_picture;
        layout.addView(view);
        TextView tv_label = new TextView(context);
        tv_label.setTextColor(Color.GRAY);
        tv_label.setPadding(10,0,0,0);
        tv_label.setTypeface(tv_label.getTypeface(), Typeface.BOLD);
        if(description!= null && !description.isEmpty()) {
            tv_label.setText(description);
        }
        if(required)
            tv_label.setText(tv_label.getText().toString()+" (required)");
        layout.addView(tv_label);
        layout.setPadding(10,0,30,10);
        return layout;
    }

    public void startImageCapture(int requestCode, Context context){
       // if (context.isFocusing) return;
        //context.isFocusing = true;
        //Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
       // Intent intent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        //intent.setType("image/*");
        //((Activity)context).startActivityForResult(intent, requestCode);
        try {
            ImagePicker.create((Activity) context)
                    .single().folderMode(true).imageTitle("Pilih Gambar")
                    .imageDirectory("Camera")
                    .start(requestCode);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public Object result() {
        return path;
    }

    @Override
    public void activityResult(int resultCode, Intent data, Context context) {
        if(resultCode == RESULT_OK && data!=null) {
            Log.d("picture", "result");
            //updateImageView(data.getData(), (ImageView) view, context);
            ArrayList<Image> images = (ArrayList<Image>) ImagePicker.getImages(data);
            if(images.size() > 0){
                Image img = images.get(0);
                Log.d("picture", img.getPath());
                path = img.getPath();
                updateImageView(context);
            }else{
                filled = false;
            }
        }
    }

    public void updateImageView(Context context){
        if(!path.isEmpty() && (new File(path)).exists()) {
            filled = true;
            Uri imageUri = Uri.fromFile(new File(path));
            ImageView iv_capture_image = (ImageView) view;

            InputStream imageStream = null;
            try {
                imageStream = context.getContentResolver().openInputStream(imageUri);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            Bitmap photo = BitmapFactory.decodeStream(imageStream);
            Log.d("picture", imageUri.toString());
            if (photo.getWidth() > photo.getHeight())
                Picasso.with(context).load(imageUri.toString()).resize(Utils.getScreenWidth(context), Utils.getScreenWidth(context) * 4 / 3).centerInside().into(iv_capture_image);
            else
                Picasso.with(context).load(imageUri.toString()).rotate(270).resize(Utils.getScreenWidth(context), Utils.getScreenWidth(context) * 4 / 3).centerInside().into(iv_capture_image);
        } else {
            filled = false;
            path="";
        }
    }

    public Bitmap getBitmapFromPath(String path){
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        Bitmap bitmap = BitmapFactory.decodeFile(path,bmOptions);
        return bitmap;
    }
    /*
    public void updateImageView(Bitmap photo, ImageView iv_capture_image, Context context){
        if(photo.getWidth()>photo.getHeight())
            Picasso.with(context).load(getImageUri(photo, context)).resize(Utils.getScreenWidth(context), Utils.getScreenWidth(context)*4/3).centerInside().into(iv_capture_image);
        else
            Picasso.with(context).load(getImageUri(photo, context)).rotate(270).resize(Utils.getScreenWidth(context), Utils.getScreenWidth(context)*4/3).centerInside().into(iv_capture_image);
    }

    */
    public Uri getImageUri(Bitmap photo, Context context) {
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), photo, "Title", null);
        return Uri.parse(path);
    }

    public String getPath(){return path; }
    public void setPath(String path){this.path = path;}
    public boolean isFilled() {return filled; }
}
