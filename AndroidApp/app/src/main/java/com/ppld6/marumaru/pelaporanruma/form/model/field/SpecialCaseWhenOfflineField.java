package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.content.Context;
import android.content.Intent;
import android.view.View;

/**
 * Created by jefly on 05/04/17.
 */

public abstract class SpecialCaseWhenOfflineField extends FormField {

    public SpecialCaseWhenOfflineField(int id, String label, String description, String placeholder, boolean required, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
    }
    public SpecialCaseWhenOfflineField(int id, String name, String label, String description, String placeholder, boolean required, int order, boolean enabled) {
        super(id, name, label, description, placeholder, required, order, enabled);
    }


    public abstract boolean validate();
    public abstract View render(Context context);
    public abstract Object result();
    public abstract void activityResult(int resultCode, Intent data, Context context);

}
