package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.text.Html;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by edison on 05/04/17.
 */

public class LongTextField extends FormField {
    private int maxLength;

    public LongTextField(int id, String label, String description, String placeholder, boolean required, int maxLength, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
        this.maxLength = maxLength;
    }
    public LongTextField(int id, String name, String label, String description, String placeholder, boolean required, int maxLength, int order, boolean enabled) {
        super(id, name, label, description, placeholder, required, order, enabled);
        this.maxLength = maxLength;
    }


    public boolean validate() {
        if(required) {
            int errType = 1;
            if (((String) result()).isEmpty()) {
                errorCode = " (" + label.charAt(0) + id + 0 + errType + ")";
                return false;
            }
            errType++;
            if (((String) result()).length() > maxLength) {
                errorCode = " (" + label.charAt(0) + id + 0 + errType + ")";
                return false;
            }
        }
        return true;
    }

    @Override
    public View render(Context context) {
        LinearLayout layout                     = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams  = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(10,20,30,10);
        layout.setLayoutParams(layoutParams);
        layout.setOrientation(LinearLayout.VERTICAL);

        EditText et_ltf = new EditText(context);
        if(!enabled){
            et_ltf.setFocusable(false);
            et_ltf.setEnabled(false);
        }
        et_ltf.setHint(label);
        et_ltf.setHintTextColor(Color.GRAY);
        et_ltf.setMinLines(4);
        view = et_ltf;
        TextInputLayout til_et_ltf = new TextInputLayout(context);
        til_et_ltf.addView(view);
        view = til_et_ltf;
        layout.addView(view);
        TextView tv_label = new TextView(context);
        tv_label.setTextColor(Color.GRAY);
        tv_label.setPadding(10,0,0,0);
        tv_label.setTypeface(tv_label.getTypeface(), Typeface.BOLD);
        if(description!= null && !description.isEmpty()) {
            tv_label.setText(description);
        }
        if(required)
            tv_label.setText(tv_label.getText().toString()+" (required)");
        layout.addView(tv_label);
        layout.setPadding(10,0,30,10);
        return layout;
    }

    @Override
    public Object result() {
        return ((TextInputLayout)view).getEditText().getText().toString();
    }

    public void setView(View view) { this.view = view; }
}
