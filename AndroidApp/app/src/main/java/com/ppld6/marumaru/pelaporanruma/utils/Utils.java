package com.ppld6.marumaru.pelaporanruma.utils;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ppld6.marumaru.pelaporanruma.login.LoginActivity;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by jefly on 28/03/17.
 */

public class Utils {
    public static int getScreenWidth(Context context) {
        return context.getResources().getSystem().getDisplayMetrics().widthPixels;
    }


    public static boolean isLoggedIn(Context context){
        FirebaseAuth auth = FirebaseAuth.getInstance();
        return auth.getCurrentUser() != null;
    }


    public static String getTimestampStringByTimeMillis(long timeMillis) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.setTimeInMillis(timeMillis);
        String date = DateFormat.format("dd-MM-yyyy hh:mm:ss", cal).toString();
        return date;
    }

    public static String getTimestampHourMinutesStringByTimeMillis(long timeMillis) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.setTimeInMillis(timeMillis);
        String date = DateFormat.format("hh:mm", cal).toString();
        return date;
    }

    public static String getDateStringByTimeMillis(long timeMillis){
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.set(Calendar.HOUR_OF_DAY, 24);
        cal.setTimeInMillis(timeMillis);
        String date = DateFormat.format("dd-MM-yyyy", cal).toString();
        return date;
    }

    public static FirebaseUser getUser(){
        return FirebaseAuth.getInstance().getCurrentUser();
    }
    public static void checkActiveUser(final Context context){
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("users/"+uid);
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(!dataSnapshot.child("active").getValue(Boolean.class)){
                    FirebaseAuth.getInstance().signOut();
                    context.startActivity(new Intent(context, LoginActivity.class));
                }
                if(FirebaseAuth.getInstance().getCurrentUser() != null) {
                    UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                            .setDisplayName(dataSnapshot.child("name").getValue(String.class))
                            .build();
                    FirebaseAuth.getInstance().getCurrentUser().updateProfile(profileUpdates);
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
