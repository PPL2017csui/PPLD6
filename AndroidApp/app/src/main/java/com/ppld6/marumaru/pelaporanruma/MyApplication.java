package com.ppld6.marumaru.pelaporanruma;

import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.google.firebase.FirebaseApp;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

/**
 * Created by jefly on 31/03/17.
 */

public class MyApplication extends MultiDexApplication {
    private DatabaseReference connectedRef;
    private FirebaseDatabase database;
    private static MyApplication mInstance;
    static boolean persistenceInitialized = false;


    @Override
    public void onCreate() {

        super.onCreate();
        FirebaseApp.initializeApp(this);
        database = FirebaseDatabase.getInstance();
        if (!persistenceInitialized)
        {
            database.setPersistenceEnabled(true);
            persistenceInitialized = true;
        }
        connectedRef = database.getReference(".info/connected");
        mInstance = this;
//        connectedRef = FirebaseDatabase.getInstance().getReference(".info/connected");
//        final FirebaseDatabase database = FirebaseDatabase.getInstance();
        final DatabaseReference myConnectionsRef = database.getReference("connection");
        connectedRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                DatabaseReference con = myConnectionsRef;
                con.setValue(Boolean.TRUE);
                con.onDisconnect().removeValue();
            }

            @Override
            public void onCancelled(DatabaseError error) {
                System.out.println("The read failed: " + error.getCode());
            }
        });
    }

    public void connectionListener(ValueEventListener listener){
        connectedRef.addValueEventListener(listener);
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }


}
