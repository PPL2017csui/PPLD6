package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ricky on 05/04/17.
 */

public class PhoneNumberField extends FormField {

    public PhoneNumberField(int id, String label, String description, String placeholder, boolean required, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
    }

    public PhoneNumberField(int id, String name, String label, String description, String placeholder, boolean required, int order, boolean enabled) {
        super(id, name, label, description, placeholder, required, order, enabled);
    }

    @Override
    public boolean validate() {
        if(required) {
            int errType = 1;
            String content = this.result();
            if (content.equals("08")){
                errorCode = " (" + label.charAt(0) + "" + id + 0 + 1 + ")";
                return false;
            }
            errType++;
            if (content.length() > 13) {
                errorCode = " (" + label.charAt(0) + "" + id + 0 + errType + ")";
                return false;
            }
            errType++;
            Pattern p = Pattern.compile("08[0-9-+]+");
            Matcher m = p.matcher(content);
            if (!m.matches()) {
                errorCode = " (" + label.charAt(0) + "" + id + 0 + errType + ")";
                return false;
            }
        }
        return true;
    }

    @Override
    public View render(Context context) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0,20,0,0);
        layout.setLayoutParams(layoutParams);


        final EditText et_pnf = new EditText(context);
        if(!enabled){
            et_pnf.setFocusable(false);
            et_pnf.setEnabled(false);
        }
        et_pnf.setId(id);
        et_pnf.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        et_pnf.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_pnf.setText("08");

        et_pnf.setHint(label);
        et_pnf.setHintTextColor(Color.GRAY);

        Selection.setSelection(et_pnf.getText(), et_pnf.getText().length());
        et_pnf.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Do nothing, auto generated
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // Do nothing, auto generated
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!s.toString().contains("08")) {
                    et_pnf.setText("08");
                    Selection.setSelection(et_pnf.getText(), et_pnf.getText().length());
                }
            }
        });
        view = et_pnf;
        TextInputLayout til_et_stf = new TextInputLayout(context);
        til_et_stf.addView(view);
        view = til_et_stf;
        layout.addView(view);
        TextView tv_label = new TextView(context);
        tv_label.setTextColor(Color.GRAY);
        tv_label.setPadding(10,0,0,0);
        tv_label.setTypeface(tv_label.getTypeface(), Typeface.BOLD);
        if(description!= null && !description.isEmpty()) {
            tv_label.setText(description);
        }
        if(required)
            tv_label.setText(tv_label.getText().toString()+" (required)");
        layout.addView(tv_label);
        layout.setPadding(10,0,30,10);
        return layout;
    }

    @Override
    public String result() {
        return ((TextInputLayout)view).getEditText().getText().toString();
    }
}
