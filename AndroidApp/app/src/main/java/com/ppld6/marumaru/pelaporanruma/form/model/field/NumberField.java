package com.ppld6.marumaru.pelaporanruma.form.model.field;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.Selection;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ricky on 05/04/17.
 */

public class NumberField extends FormField {
    protected int minValue = Integer.MIN_VALUE;
    protected int maxValue = Integer.MAX_VALUE;

    public NumberField(int id, String label, String description, String placeholder, boolean required, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
    }


    public NumberField(int id, String name, String label, String description, String placeholder, boolean required, int order, boolean enabled) {
        super(id, name, label, description, placeholder, required, order, enabled);
    }

    public NumberField(int id, String label, String description, String placeholder, boolean required, int min, int max, boolean enabled) {
        super(id, label, description, placeholder, required, enabled);
        this.minValue = min;
        this.maxValue = max;
    }

    public NumberField(int id, String name, String label, String description, String placeholder, boolean required, int min, int max, int order, boolean enabled) {
        super(id, name, label, description, placeholder, required, order, enabled);
        this.minValue = min;
        this.maxValue = max;
    }

    @Override
    public boolean validate() {
        String content = this.result();
        if (required) {
            int errType = 1;
            if((content == null || content.equals(""))){
                errorCode = " (" + label.charAt(0) + id + 0 + errType + ")";
                return false;
            }
            errType++;
            Pattern p = Pattern.compile("[0-9-+]+");
            Matcher m = p.matcher(content);
            if (!m.matches()) {
                errorCode = " (" + label.charAt(0) + id + 0 + errType + ")";
                return false;
            }
            errType++;
            BigInteger num = new BigInteger(content);
            BigInteger low = new BigInteger(minValue + "");
            BigInteger high = new BigInteger(maxValue + "");
            if (!(low.compareTo(num) <= 0 && num.compareTo(high) <= 0)) {
                errorCode = " (" + label.charAt(0) + id + 0 + errType + ")";
                return false;
            }
        }
        return true;
    }

    @Override
    public View render(Context context) {
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0,20,0,0);
        layout.setLayoutParams(layoutParams);


        final EditText et_field = new EditText(context);
        if(!enabled){
            et_field.setFocusable(false);
            et_field.setEnabled(false);
        }
        et_field.setId(id);
        et_field.setRawInputType(InputType.TYPE_CLASS_NUMBER);
        et_field.setInputType(InputType.TYPE_CLASS_NUMBER);
        et_field.setHint(label);
        et_field.setHintTextColor(Color.GRAY);
        view = et_field;
        TextInputLayout til_et_lf = new TextInputLayout(context);
        til_et_lf.addView(view);
        view = til_et_lf;
        layout.addView(view);
        TextView tv_label = new TextView(context);
        tv_label.setTextColor(Color.GRAY);
        tv_label.setPadding(10,0,0,0);
        tv_label.setTypeface(tv_label.getTypeface(), Typeface.BOLD);
        if(description!= null && !description.isEmpty()) {
            tv_label.setText(description);
        }
        if(required)
            tv_label.setText(tv_label.getText().toString()+" (required)");
        layout.addView(tv_label);
        layout.setPadding(10,0,30,10);
        return layout;
    }

    @Override
    public String result() {
        return ((TextInputLayout) view).getEditText().getText().toString();
    }
    public void setView(View view) { this.view = view; }
}
